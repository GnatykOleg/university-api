# University API

University м is a CRUD server that is used with the NestJS framework.

## Installation

#### To use this project, follow the instructions below:

-   Clone the repository to your local machine. `git clone [repository_url]`

-   Change to the project directory. `cd [dir_name]`

-   Install the required dependencies with `npm install`.

### Running the linter the code

-   You can use a possible command to check code style and formatting: `npm run lint`

-   Fix code style bug: `npm run lint:fix`

### Run application using Docker & Docker-compose

-   Choose Dockerfile or Dockerfile.production and set in docker-compose.yml:
    build:
    context: .
    dockerfile: ./Dockerfile OR ./Dockerfile.production
-   Use command docker-compose up (optional flag --build, if docker-compose has changes)
-   Use command docker-compose down for stop container

### Run TypeORM with migrations

-   Run TypeORM CLI with TypeScript support and load environment variables from .env file `npm run typeorm`

-   Configuring TypeORM using the specified database configuration file. `npm run typeform:config`

-   Create a new migration with the specified name `npm run migrations:create`

-   Generate new migration based on current database state `npm run migrations:generate --name=MigrationName`

-   Revert last applied migration `npm run migrations:revert`

-   Apply all available migrations in database `npm run migrations:run`

## Swagger documentation

-   After running the application, you can access the API documentation using Swagger. Open a web browser and go to http://localhost:APP_PORT/api/v1/docs (Where APP_PORT is the port you specified in .env or 4000 by default) to view available endpoints and API requests.

-   Make sure the application is running before accessing the API or viewing the Swagger documentation.
