module.exports = {
    printWidth: 80 /* The length of the string that prettier will wrap */,

    tabWidth: 4 /* Number of spaces per indent level. */,

    semi: true /* Semicolons at the end of statements */,

    singleQuote: true /* Single quotes instead of double quotes singleQuote: true */,

    trailingComma:
        'all' /* Trailing commas wherever possible. As an example arrays, objects */,

    arrowParens:
        'always' /* Leave parentheses at the only parameter of the arrow function, for typing */,
};
