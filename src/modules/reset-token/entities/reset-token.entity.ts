import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'reset_token' })
export class ResetToken extends CoreEntity {
    @ApiProperty(apiPropertyOptions.JWT_TOKEN)
    @Column({ type: 'text', nullable: false })
    token: string;

    @ApiProperty(apiPropertyOptions.COMMON_ID)
    @Column({
        type: 'uuid',
        nullable: false,
        name: 'user_id',
    })
    userId: string;

    @OneToOne(() => Lector, (lector) => lector.resetToken, {
        eager: false,
        onDelete: 'CASCADE',
        onUpdate: 'NO ACTION',
    })
    @JoinColumn({ name: 'user_id' })
    lector: Lector;
}
