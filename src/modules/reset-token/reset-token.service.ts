import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import crypto from 'crypto';
import { Repository } from 'typeorm';

import { ResetToken } from './entities/reset-token.entity';

@Injectable()
export class ResetTokenService {
    private readonly logger: Logger;

    constructor(
        @InjectRepository(ResetToken)
        private readonly resetTokenRepository: Repository<ResetToken>,
    ) {
        this.logger = new Logger(ResetTokenService.name);
    }

    async generateResetToken(userId: string): Promise<string> {
        const resetToken = crypto.randomBytes(32).toString('hex');

        await this.resetTokenRepository.save({
            token: resetToken,
            userId,
        });

        return resetToken;
    }

    async findResetToken(resetToken: string): Promise<ResetToken> {
        const foundToken = this.resetTokenRepository.findOneBy({
            token: resetToken,
        });

        return foundToken;
    }

    async findResetTokenByUserId(userId: string): Promise<ResetToken> {
        const foundToken = this.resetTokenRepository.findOneBy({ userId });

        return foundToken;
    }

    async deleteResetTokenByUserId(userId: string): Promise<void> {
        this.resetTokenRepository.delete({ userId });
    }
}
