import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ResetToken } from './entities/reset-token.entity';
import { ResetTokenService } from './reset-token.service';

@Module({
    imports: [TypeOrmModule.forFeature([ResetToken])],
    providers: [ResetTokenService],
    exports: [ResetTokenService],
})
export class ResetTokenModule {}
