export interface ISendUserResetPaswordLinkParams {
    resetToken: string;
    name: string;
    email: string;
}
