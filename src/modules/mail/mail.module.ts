import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';

import mailerAsyncFactoryConfig from '../../configs/auth/mail-async-factory.config';
import { MailService } from './mail.service';

@Module({
    imports: [MailerModule.forRootAsync(mailerAsyncFactoryConfig)],
    providers: [MailService],
    exports: [MailService],
})
export class MailModule {}
