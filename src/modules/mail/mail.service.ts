import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MailerService } from '@nestjs-modules/mailer';

import { ISendUserResetPaswordLinkParams } from './interfaces/send-user-reset-password-link.params.interface';

@Injectable()
export class MailService {
    constructor(
        private readonly mailerService: MailerService,
        private readonly configService: ConfigService,
    ) {}

    async sendUserResetPassowrdLink({
        resetToken,
        name,
        email,
    }: ISendUserResetPaswordLinkParams): Promise<void> {
        const FRONTEND_BASE_URL =
            this.configService.get<string>('FRONTEND_BASE_URL');

        const url = `${FRONTEND_BASE_URL}/reset-password?token=${resetToken}`;

        await this.mailerService.sendMail({
            to: email,
            subject: 'Reset your password',
            template: './reset-password-request',
            context: {
                name,
                url,
            },
        });
    }
}
