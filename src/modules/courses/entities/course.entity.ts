import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, ManyToMany, OneToMany } from 'typeorm';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { Student } from '../../students/entitites/student.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
    @ApiProperty(apiPropertyOptions.COURSE_NAME)
    @Column({ type: 'varchar', nullable: false, length: 50 })
    @Index()
    name: string;

    @ApiProperty(apiPropertyOptions.COURSE_DESCRIPTION)
    @Column({ type: 'text', nullable: false })
    description: string;

    @ApiProperty(apiPropertyOptions.COURSE_HOURS)
    @Column({ type: 'integer', nullable: false })
    hours: number;

    @OneToMany(() => Mark, (mark) => mark.course)
    marks: Mark[];

    @ManyToMany(() => Lector, (lector) => lector.courses, {
        onDelete: 'CASCADE',
        onUpdate: 'NO ACTION',
    })
    lectors: Lector[];

    @ApiProperty({ type: () => [Student] })
    @ManyToMany(() => Student, (student) => student.courses, {
        onDelete: 'CASCADE',
        onUpdate: 'NO ACTION',
    })
    students?: Student[];
}
