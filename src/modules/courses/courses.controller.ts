import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UseGuards,
} from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiBody,
    ApiConflictResponse,
    ApiCreatedResponse,
    ApiInternalServerErrorResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiOperation,
    ApiParam,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import ENDPOINTS from '../app/constants/endpoints.constant';
import { ConflictErrorResponseDto } from '../app/dto/errors/conflict-error.response.dto';
import { InternalServerErrorResponseDto } from '../app/dto/errors/internal-server-error.response.dto';
import { NotFoundErrorResponseDto } from '../app/dto/errors/not-found-error.response.dto';
import { UnauthorizeErrordResponseDto } from '../app/dto/errors/unauthorized-error.response.dto';
import { ValidationFailedErrorResponseDto } from '../app/dto/errors/validation-failed-error.response.dto';
import { IdParamDto } from '../app/dto/params/id-param.dto';
import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateApiParamOptionsForId } from '../app/utils';
import { AuthGuard } from '../auth/guards/auth.guard';
import { CoursesService } from './courses.service';
import { CreateCourseRequestDto } from './dto/create-course.request.dto';
import { FindAllCoursesQueryDto } from './dto/find-all-courses.query.dto';
import { FindAllCoursesResponseDto } from './dto/find-all-courses.response.dto';
import { UpdateCourseByIdRequestDto } from './dto/update-course-by-id.request.dto';
import { Course } from './entities/course.entity';

@UseGuards(AuthGuard)
@ApiBearerAuth('access-token')
@ApiTags('Courses')
@Controller(ENDPOINTS.COURSES)
export class CoursesController {
    constructor(private readonly coursesService: CoursesService) {}

    // Find All Courses
    @ApiOperation({ summary: 'Find all courses with filter query' })
    @ApiOkResponse({
        description: 'Receives either an array of courses, or an empty array',
        type: FindAllCoursesResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get()
    async findAllCourses(
        @Query() query: FindAllCoursesQueryDto,
    ): Promise<FindAllCoursesResponseDto> {
        const allCourses = await this.coursesService.findAllCourses(query);

        return allCourses;
    }

    // Find Course By Id
    @ApiOperation({ summary: 'Find course by id' })
    @ApiParam(generateApiParamOptionsForId('course', 'courseId'))
    @ApiOkResponse({
        description: 'The course has been successfully found',
        type: Course,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.COURSE_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get(':id')
    async findGroupById(@Param() params: IdParamDto): Promise<Course> {
        const courseId = params.id;

        const foundCourse = await this.coursesService.findCourseById(courseId);

        return foundCourse;
    }

    // Create Course
    @ApiOperation({ summary: 'Create course' })
    @ApiCreatedResponse({
        description: 'The course has been successfully created',
        type: Course,
    })
    @ApiConflictResponse({
        description: HttpMessagesEnum.COURSE_ALREDY_EXIST,
        type: ConflictErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Post()
    async createCourse(
        @Body() createCourseRequestDto: CreateCourseRequestDto,
    ): Promise<Course> {
        const createdCourse = await this.coursesService.createCourse(
            createCourseRequestDto,
        );

        return createdCourse;
    }

    // Update Course By Id
    @ApiOperation({ summary: 'Update course by id' })
    @ApiBody({ required: false, type: UpdateCourseByIdRequestDto })
    @ApiParam(generateApiParamOptionsForId('course', 'courseId'))
    @ApiOkResponse({
        description: 'The course has been successfully updated',
        type: Course,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.COURSE_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Patch(':id')
    async updateGroupById(
        @Param() params: IdParamDto,
        @Body() updateGroupByIdRequestDto: UpdateCourseByIdRequestDto,
    ): Promise<Course> {
        const courseId = params.id;

        const updatedCourse = await this.coursesService.updateCourseById({
            courseId,
            updateGroupByIdRequestDto,
        });

        return updatedCourse;
    }

    // Delete course By Id
    @ApiOperation({ summary: 'Delete course by id' })
    @ApiParam(generateApiParamOptionsForId('course', 'courseId'))
    @ApiNoContentResponse({
        description: 'The course has been successfully deleted',
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.COURSE_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteCourseById(@Param() params: IdParamDto): Promise<void> {
        const courseId = params.id;

        await this.coursesService.deleteCourseById(courseId);
    }
}
