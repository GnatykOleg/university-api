import {
    ConflictException,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateLikeFindOperator } from '../app/utils';
import { CreateCourseRequestDto } from './dto/create-course.request.dto';
import { FindAllCoursesQueryDto } from './dto/find-all-courses.query.dto';
import { FindAllCoursesResponseDto } from './dto/find-all-courses.response.dto';
import { Course } from './entities/course.entity';
import { IUpdateCourseByIdParams } from './interfaces/update-course-by-id.params.interface';

@Injectable()
export class CoursesService {
    constructor(
        @InjectRepository(Course)
        private readonly coursesRepository: Repository<Course>,
    ) {}

    async findAllCourses(
        query: FindAllCoursesQueryDto,
    ): Promise<FindAllCoursesResponseDto> {
        const { sort_field, sort_order, name, page, per_page } = query;

        const [data, total] = await this.coursesRepository.findAndCount({
            where: {
                name: generateLikeFindOperator(name),
            },

            order: { [sort_field]: sort_order },

            relations: { students: true },

            skip: per_page * (page - 1),
            take: per_page,
        });

        return { data, total };
    }

    async findCourseById(courseId: string): Promise<Course> {
        const foundCourse = this.coursesRepository.findOne({
            where: { id: courseId },
            relations: { students: true },
        });

        if (!foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        return foundCourse;
    }

    async createCourse(
        createCourseRequestDto: CreateCourseRequestDto,
    ): Promise<Course> {
        const { name } = createCourseRequestDto;

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({ name });

        if (foundCourse)
            throw new ConflictException(HttpMessagesEnum.COURSE_ALREDY_EXIST);

        const createdCourse = await this.coursesRepository.save(
            createCourseRequestDto,
        );

        const course = this.coursesRepository.findOne({
            where: { id: createdCourse.id },
            relations: { students: true },
        });

        return course;
    }

    async updateCourseById({
        courseId,
        updateGroupByIdRequestDto,
    }: IUpdateCourseByIdParams): Promise<Course> {
        const updatedCourse: UpdateResult = await this.coursesRepository.update(
            courseId,
            updateGroupByIdRequestDto,
        );

        if (!updatedCourse.affected)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        const foundCourse = this.coursesRepository.findOne({
            where: { id: courseId },
            relations: { students: true },
        });

        return foundCourse;
    }

    async deleteCourseById(courseId: string): Promise<void> {
        const deletedCourse: DeleteResult = await this.coursesRepository.delete(
            courseId,
        );

        if (!deletedCourse.affected)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);
    }
}
