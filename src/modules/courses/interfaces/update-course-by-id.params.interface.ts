import { UpdateCourseByIdRequestDto } from '../dto/update-course-by-id.request.dto';

export interface IUpdateCourseByIdParams {
    courseId: string;
    updateGroupByIdRequestDto: UpdateCourseByIdRequestDto;
}
