import { ApiProperty } from '@nestjs/swagger';

import { Course } from '../entities/course.entity';

export class FindAllCoursesResponseDto {
    @ApiProperty({
        type: [Course],
        description: 'Array of Course objects or an empty array',
    })
    data: Course[] | [];

    @ApiProperty({ default: 1 })
    total: number;
}
