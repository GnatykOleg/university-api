import { PartialType } from '@nestjs/swagger';

import { CreateCourseRequestDto } from './create-course.request.dto';

export class UpdateCourseByIdRequestDto extends PartialType(
    CreateCourseRequestDto,
) {}
