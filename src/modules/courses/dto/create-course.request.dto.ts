import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsInt, Max, MaxLength, Min, MinLength } from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CLASS_VALIDATOR_VALUES from '../../app/constants/class-validator-values.constant';
import { lowerCaseAndTrim } from '../../app/utils';

export class CreateCourseRequestDto {
    @ApiProperty(apiPropertyOptions.COURSE_NAME)
    @Transform(lowerCaseAndTrim)
    @MinLength(CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH)
    @MaxLength(CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH)
    name: string;

    @ApiProperty(apiPropertyOptions.COURSE_DESCRIPTION)
    @Transform(lowerCaseAndTrim)
    @MinLength(CLASS_VALIDATOR_VALUES.TEXT_MIN_LENGTH)
    description: string;

    @ApiProperty(apiPropertyOptions.COURSE_HOURS)
    @IsInt()
    @Min(CLASS_VALIDATOR_VALUES.INT_HOURS_MIN)
    @Max(CLASS_VALIDATOR_VALUES.INT_HOURS_MAX)
    hours: number;
}
