export interface IAddCourseToLectorByIdParams {
    lectorId: string;
    courseId: string;
}
