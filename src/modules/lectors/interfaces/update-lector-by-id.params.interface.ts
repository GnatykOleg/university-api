import { UpdateLectorByIdRequestDto } from '../dto/update-lector.request.dto';

export interface IUpdateLectorByIdParams {
    lectorId: string;
    updateLectorByIdRequestDto: UpdateLectorByIdRequestDto;
}
