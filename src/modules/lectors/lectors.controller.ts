import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiBody,
    ApiConflictResponse,
    ApiCreatedResponse,
    ApiInternalServerErrorResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiOperation,
    ApiParam,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import ENDPOINTS from '../app/constants/endpoints.constant';
import { InternalServerErrorResponseDto } from '../app/dto/errors/internal-server-error.response.dto';
import { NotFoundErrorResponseDto } from '../app/dto/errors/not-found-error.response.dto';
import { UnauthorizeErrordResponseDto } from '../app/dto/errors/unauthorized-error.response.dto';
import { ValidationFailedErrorResponseDto } from '../app/dto/errors/validation-failed-error.response.dto';
import { IdParamDto } from '../app/dto/params/id-param.dto';
import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateApiParamOptionsForId } from '../app/utils';
import { AuthGuard } from '../auth/guards/auth.guard';
import { CreateLectorRequestDto } from './dto/create-lector.request.dto';
import { FindAllLectorsQueryDto } from './dto/find-all-lectors.query.dto';
import { FindAllLectorsResponseDto } from './dto/find-all-lectors.response.dto';
import { UpdateLectorByIdRequestDto } from './dto/update-lector.request.dto';
import { Lector } from './entities/lector.entity';
import { LectorsService } from './lectors.service';

@UseGuards(AuthGuard)
@ApiBearerAuth('access-token')
@ApiTags('Lectors')
@UseInterceptors(ClassSerializerInterceptor)
@Controller(ENDPOINTS.LECTORS)
export class LectorsController {
    constructor(private readonly lectorsService: LectorsService) {}

    // Find All Lectors
    @ApiOperation({ summary: 'Find all lectors with filter query' })
    @ApiOkResponse({
        description: 'Receives an array of lectors, or an empty array',
        type: FindAllLectorsResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get()
    async findAllLectors(
        @Query() query: FindAllLectorsQueryDto,
    ): Promise<FindAllLectorsResponseDto> {
        const allLectors = await this.lectorsService.findAllLectors(query);

        return allLectors;
    }

    // Find Lector By Id
    @ApiOperation({ summary: 'Find lector by id' })
    @ApiParam(generateApiParamOptionsForId('lector', 'lectorId'))
    @ApiOkResponse({
        description: 'The lector has been successfully found',
        type: Lector,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.LECTOR_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get(':id')
    async findLectorById(@Param() params: IdParamDto): Promise<Lector> {
        const lectorId = params.id;

        const foundLector = await this.lectorsService.findLectorById(lectorId);

        return foundLector;
    }

    @ApiOperation({ summary: 'Create lector' })
    @ApiCreatedResponse({
        description: 'The lector has been successfully created',
        type: Lector,
    })
    @ApiConflictResponse({ description: HttpMessagesEnum.LECTOR_ALREDY_EXIST })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Post()
    async createLector(
        @Body() createLectorRequestDto: CreateLectorRequestDto,
    ): Promise<Lector> {
        const createdLector = await this.lectorsService.createLector(
            createLectorRequestDto,
        );

        return createdLector;
    }

    // Update Lector By Id
    @ApiOperation({ summary: 'Update lector by id' })
    @ApiBody({ required: false, type: UpdateLectorByIdRequestDto })
    @ApiParam(generateApiParamOptionsForId('lector', 'lectorId'))
    @ApiOkResponse({
        description: 'The lector has been successfully updated',
        type: Lector,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.LECTOR_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Patch(':id')
    async updateLectorById(
        @Param() params: IdParamDto,
        @Body() updateLectorByIdRequestDto: UpdateLectorByIdRequestDto,
    ): Promise<Lector> {
        const lectorId = params.id;

        const updatedLector = await this.lectorsService.updateLectorById({
            lectorId,
            updateLectorByIdRequestDto,
        });

        return updatedLector;
    }

    // Delete Lector By Id
    @ApiOperation({ summary: 'Delete lector by id' })
    @ApiParam(generateApiParamOptionsForId('lector', 'lectorId'))
    @ApiNoContentResponse({
        description: 'The lector has been successfully deleted',
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.LECTOR_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteLectorById(@Param() params: IdParamDto): Promise<void> {
        const lectorId = params.id;

        await this.lectorsService.deleteLectorById(lectorId);
    }
}
