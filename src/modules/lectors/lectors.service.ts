import {
    ConflictException,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';

import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { encodePassword, generateLikeFindOperator } from '../app/utils';
import { Course } from '../courses/entities/course.entity';
import { CreateLectorRequestDto } from './dto/create-lector.request.dto';
import { FindAllLectorsQueryDto } from './dto/find-all-lectors.query.dto';
import { FindAllLectorsResponseDto } from './dto/find-all-lectors.response.dto';
import { Lector } from './entities/lector.entity';
import { LectorCourse } from './entities/lector-course.entity';
import { IUpdateLectorByIdParams } from './interfaces/update-lector-by-id.params.interface';

@Injectable()
export class LectorsService {
    constructor(
        @InjectRepository(Lector)
        private readonly lectorsRepository: Repository<Lector>,

        @InjectRepository(Course)
        private readonly coursesRepository: Repository<Course>,

        @InjectRepository(LectorCourse)
        private readonly lectorCourseRepository: Repository<LectorCourse>,
    ) {}

    async findAllLectors(
        query: FindAllLectorsQueryDto,
    ): Promise<FindAllLectorsResponseDto> {
        const { sort_field, sort_order, name, surname, page, per_page } = query;

        const [data, total] = await this.lectorsRepository.findAndCount({
            where: {
                name: generateLikeFindOperator(name),
                surname: generateLikeFindOperator(surname),
            },

            order: { [sort_field]: sort_order },

            skip: per_page * (page - 1),
            take: per_page,
        });

        return { data, total };
    }

    async findLectorByEmail(email: string): Promise<Lector> {
        return this.lectorsRepository.findOneBy({ email });
    }

    async findLectorById(lectorId: string): Promise<Lector> {
        const foundLector = await this.lectorsRepository.findOneBy({
            id: lectorId,
        });

        if (!foundLector)
            throw new NotFoundException(HttpMessagesEnum.LECTOR_NOT_FOUND);

        return foundLector;
    }

    async createLector(
        createLectorRequestDto: CreateLectorRequestDto,
    ): Promise<Lector> {
        const { email, password, courseId, name, surname } =
            createLectorRequestDto;

        const foundLector: Lector | null =
            await this.lectorsRepository.findOneBy({ email });

        if (foundLector)
            throw new ConflictException(HttpMessagesEnum.LECTOR_ALREDY_EXIST);

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({
                id: courseId,
            });

        if (courseId && !foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        const encodedPassword = await encodePassword(password);

        const createdLector = await this.lectorsRepository.save({
            email,
            password: encodedPassword,
            name,
            surname,
        });

        if (courseId && foundCourse)
            await this.lectorCourseRepository.save({
                courseId,
                lectorId: createdLector.id,
            });

        const lector = this.findLectorByEmail(createdLector.email);

        return lector;
    }

    async updateLectorById({
        lectorId,
        updateLectorByIdRequestDto,
    }: IUpdateLectorByIdParams): Promise<Lector> {
        const { courseId, email, name, password, surname } =
            updateLectorByIdRequestDto;

        const lectorToUpdate: Lector | null =
            await this.lectorsRepository.findOneBy({
                id: lectorId,
            });

        if (!lectorToUpdate)
            throw new NotFoundException(HttpMessagesEnum.LECTOR_NOT_FOUND);

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({
                id: courseId,
            });

        if (courseId && !foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        const encodedPassword = password
            ? await encodePassword(password)
            : undefined;

        await this.lectorsRepository.update(lectorId, {
            email,
            name,
            password: encodedPassword,
            surname,
        });

        if (courseId && foundCourse)
            await this.lectorCourseRepository.save({
                courseId,
                lectorId,
            });

        const lector = this.lectorsRepository.findOneBy({
            id: lectorId,
        });

        return lector;
    }

    async deleteLectorById(lectorId: string): Promise<void> {
        const deletedLector: DeleteResult = await this.lectorsRepository.delete(
            lectorId,
        );

        if (!deletedLector.affected)
            throw new NotFoundException(HttpMessagesEnum.LECTOR_NOT_FOUND);
    }
}
