import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthGuard } from '../auth/guards/auth.guard';
import { Course } from '../courses/entities/course.entity';
import { Lector } from './entities/lector.entity';
import { LectorCourse } from './entities/lector-course.entity';
import { LectorsController } from './lectors.controller';
import { LectorsService } from './lectors.service';

@Module({
    imports: [TypeOrmModule.forFeature([Lector, Course, LectorCourse])],
    controllers: [LectorsController],
    providers: [LectorsService, AuthGuard, JwtService],
    exports: [LectorsService],
})
export class LectorsModule {}
