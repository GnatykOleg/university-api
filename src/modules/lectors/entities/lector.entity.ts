import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import {
    Column,
    Entity,
    Index,
    JoinTable,
    ManyToMany,
    OneToMany,
    OneToOne,
} from 'typeorm';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ResetToken } from '../../reset-token/entities/reset-token.entity';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
    @ApiProperty(apiPropertyOptions.PERSON_NAME_NULLABLE)
    @Column({ type: 'varchar', nullable: true, length: 50 })
    @Index()
    name: string;

    @ApiProperty(apiPropertyOptions.PERSON_SURNAME_NULLABLE)
    @Column({ type: 'varchar', nullable: true, length: 50 })
    @Index()
    surname: string;

    @ApiProperty(apiPropertyOptions.PERSON_EMAIL)
    @Column({ type: 'varchar', nullable: false, length: 50 })
    email: string;

    @Exclude()
    @Column({ type: 'text', nullable: false })
    password: string;

    @OneToMany(() => Mark, (mark) => mark.lector)
    marks: Mark[];

    @OneToOne(() => ResetToken, (resetToken) => resetToken.lector)
    resetToken: ResetToken;

    @ManyToMany(() => Course, (course) => course.lectors, {
        onDelete: 'CASCADE',
        onUpdate: 'NO ACTION',
    })
    @JoinTable({
        name: 'lector_course',
        joinColumn: {
            name: 'lector_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'course_id',
            referencedColumnName: 'id',
        },
    })
    courses: Course[];
}
