import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { Course } from '../../courses/entities/course.entity';
import { Lector } from './lector.entity';

@Entity('lector_course')
export class LectorCourse {
    @PrimaryColumn({ name: 'lector_id' })
    lectorId: string;

    @ManyToOne(() => Lector, (lector) => lector.courses)
    @JoinColumn([{ name: 'lector_id', referencedColumnName: 'id' }])
    lectors: Lector[];

    @PrimaryColumn({ name: 'course_id' })
    courseId: string;

    @ManyToOne(() => Course, (course) => course.lectors)
    @JoinColumn([{ name: 'course_id', referencedColumnName: 'id' }])
    courses: Course[];
}
