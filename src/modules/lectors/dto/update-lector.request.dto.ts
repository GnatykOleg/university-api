import { PartialType } from '@nestjs/swagger';

import { CreateLectorRequestDto } from './create-lector.request.dto';

export class UpdateLectorByIdRequestDto extends PartialType(
    CreateLectorRequestDto,
) {}
