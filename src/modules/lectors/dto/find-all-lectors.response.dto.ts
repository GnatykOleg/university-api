import { ApiProperty } from '@nestjs/swagger';

import { Lector } from '../entities/lector.entity';

export class FindAllLectorsResponseDto {
    @ApiProperty({
        type: [Lector],
        description: 'Array of Lector objects or an empty array',
    })
    data: Lector[] | [];

    @ApiProperty({ default: 1 })
    total: number;
}
