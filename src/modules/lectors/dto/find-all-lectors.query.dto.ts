import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsOptional } from 'class-validator';

import { SortPaginationQueryDto } from '../../app/dto/params/sort-pagination.query.dto';
import { lowerCaseAndTrim } from '../../app/utils';

export class FindAllLectorsQueryDto extends SortPaginationQueryDto {
    @ApiPropertyOptional()
    @Transform(lowerCaseAndTrim)
    @IsOptional()
    name?: string | undefined;

    @ApiPropertyOptional()
    @Transform(lowerCaseAndTrim)
    @IsOptional()
    surname?: string | undefined;
}
