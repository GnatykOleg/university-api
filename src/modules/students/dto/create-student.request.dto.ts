import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
    IsEmail,
    IsInt,
    IsOptional,
    IsString,
    IsUUID,
    Max,
    MaxLength,
    Min,
    MinLength,
} from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CLASS_VALIDATOR_VALUES from '../../app/constants/class-validator-values.constant';
import { lowerCaseAndTrim } from '../../app/utils';

export class CreateStudentRequestDto {
    @ApiProperty(apiPropertyOptions.PERSON_NAME)
    @Transform(lowerCaseAndTrim)
    @MinLength(CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH)
    @MaxLength(CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH)
    name: string;

    @ApiProperty(apiPropertyOptions.PERSON_SURNAME)
    @Transform(lowerCaseAndTrim)
    @MinLength(CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH)
    @MaxLength(CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH)
    surname: string;

    @ApiProperty(apiPropertyOptions.PERSON_EMAIL)
    @Transform(lowerCaseAndTrim)
    @IsEmail()
    @MinLength(CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH)
    @MaxLength(CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH)
    email: string;

    @ApiProperty(apiPropertyOptions.PERSON_AGE)
    @IsInt()
    @Min(CLASS_VALIDATOR_VALUES.INT_AGE_MIN)
    @Max(CLASS_VALIDATOR_VALUES.INT_AGE_MAX)
    age: number;

    @ApiPropertyOptional(apiPropertyOptions.GROUP_ID)
    @IsUUID()
    @IsOptional()
    @IsString()
    groupId?: string;

    @ApiPropertyOptional(apiPropertyOptions.COURSE_ID)
    @IsUUID()
    @IsOptional()
    @IsString()
    courseId?: string;
}
