import { PartialType } from '@nestjs/swagger';

import { CreateStudentRequestDto } from './create-student.request.dto';

export class UpdateStudentByIdRequestDto extends PartialType(
    CreateStudentRequestDto,
) {}
