import { ApiProperty, OmitType } from '@nestjs/swagger';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Group } from '../../groups/entities/group.entity';

export class StudentResponseDto extends CoreEntity {
    @ApiProperty(apiPropertyOptions.PERSON_NAME)
    name: string;

    @ApiProperty(apiPropertyOptions.PERSON_SURNAME)
    surname: string;

    @ApiProperty(apiPropertyOptions.PERSON_EMAIL)
    email: string;

    @ApiProperty(apiPropertyOptions.PERSON_AGE)
    age: number;

    @ApiProperty(apiPropertyOptions.PERSON_IMAGE_PATH_NULLABLE)
    imagePath: string | null;

    @ApiProperty({ nullable: true, type: Group })
    group: { name: string } | null;

    @ApiProperty({ type: () => [OmitType(Course, ['students'])] })
    courses: { name: string }[] | [];
}
