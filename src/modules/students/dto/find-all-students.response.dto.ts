import { ApiProperty } from '@nestjs/swagger';

import { StudentResponseDto } from './student.response.dto';

export class FindAllStudentsResponseDto {
    @ApiProperty({
        type: [StudentResponseDto],
        description: 'Array of Student objects or an empty array',
    })
    data: StudentResponseDto[] | [];

    @ApiProperty({ default: 1 })
    total: number;
}
