import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUUID } from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';

export class DeleteCourseFromStudentRequestDto {
    @ApiProperty(apiPropertyOptions.COURSE_ID)
    @IsUUID()
    @IsString()
    courseId: string;
}
