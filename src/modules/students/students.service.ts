import {
    BadRequestException,
    ConflictException,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateLikeFindOperator } from '../app/utils';
import { CloudinaryService } from '../cloudinary/cloudinary.service';
import { CLOUDINARY_FOLDER_NAME } from '../cloudinary/constants/cloudinary.constant';
import { Course } from '../courses/entities/course.entity';
import { Group } from '../groups/entities/group.entity';
import { CreateStudentRequestDto } from './dto/create-student.request.dto';
import { FindAllStudentsQueryDto } from './dto/find-all-students.query.dto';
import { FindAllStudentsResponseDto } from './dto/find-all-students.response.dto';
import { StudentResponseDto } from './dto/student.response.dto';
import { Student } from './entitites/student.entity';
import { StudentCourse } from './entitites/student-course.entity';
import { IAddImageToStudentByIdParams } from './interfaces/add-image-to-student-by-id.params.interface';
import { IDeleteCourseFromStudentByIdParams } from './interfaces/delete-course-from-student-by-id.params.interface';
import { IUpdateStudentByIdParams } from './interfaces/update-student-by-id.params.interface';

const studentSelectFieldsWithRelations = {
    select: {
        id: true,
        createdAt: true,
        updatedAt: true,
        name: true,
        surname: true,
        email: true,
        age: true,
        imagePath: true,
    },

    relations: { group: true, courses: true },
};

@Injectable()
export class StudentsService {
    constructor(
        @InjectRepository(Student)
        private readonly studentsRepository: Repository<Student>,

        @InjectRepository(Group)
        private readonly groupsRepository: Repository<Group>,

        @InjectRepository(Course)
        private readonly coursesRepository: Repository<Course>,

        @InjectRepository(StudentCourse)
        private readonly studentCourseRepository: Repository<StudentCourse>,

        private readonly cloudinaryService: CloudinaryService,
    ) {}

    // Temporaty method because we didn't have public_id property in student entity, for remove image from cloudinary
    private getImagePublicId(imagePath: string): string {
        const imageId = imagePath.split('/').pop().split('.')[0];

        const imagePublicId = `${CLOUDINARY_FOLDER_NAME}/${imageId}`;

        return imagePublicId;
    }

    async findAllStudents(
        query: FindAllStudentsQueryDto,
    ): Promise<FindAllStudentsResponseDto> {
        const {
            sort_field,
            sort_order,
            name,
            surname,
            page,
            per_page,
            group_name,
            course_name,
        } = query;

        const [data, total] = await this.studentsRepository.findAndCount({
            ...studentSelectFieldsWithRelations,

            where: {
                name: generateLikeFindOperator(name),
                surname: generateLikeFindOperator(surname),
                group: { name: generateLikeFindOperator(group_name) },
                courses: { name: generateLikeFindOperator(course_name) },
            },

            order: { [sort_field]: sort_order },
            skip: per_page * (page - 1),
            take: per_page,
        });

        return { data, total };
    }

    async findStudentById(studentId: string): Promise<StudentResponseDto> {
        const foundStudent: StudentResponseDto | undefined =
            await this.studentsRepository.findOne({
                ...studentSelectFieldsWithRelations,

                where: { id: studentId },
            });

        if (!foundStudent)
            throw new NotFoundException(HttpMessagesEnum.STUDENT_NOT_FOUND);

        return foundStudent;
    }

    async createStudent(
        createStudentRequestDto: CreateStudentRequestDto,
    ): Promise<StudentResponseDto> {
        const { email, groupId, courseId } = createStudentRequestDto;

        const foundStudent: Student | null =
            await this.studentsRepository.findOneBy({ email });

        if (foundStudent)
            throw new ConflictException(HttpMessagesEnum.STUDENT_ALREDY_EXIST);

        const foundGroup: Group | null = await this.groupsRepository.findOneBy({
            id: groupId,
        });

        if (groupId && !foundGroup)
            throw new NotFoundException(HttpMessagesEnum.GROUP_NOT_FOUND);

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({
                id: courseId,
            });

        if (courseId && !foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { courseId: _, ...studentEntityCreateBody } =
            createStudentRequestDto;

        const createdStudent = await this.studentsRepository.save(
            studentEntityCreateBody,
        );

        if (courseId && foundCourse)
            await this.studentCourseRepository.save({
                courseId,
                studentId: createdStudent.id,
            });

        const student = this.findStudentById(createdStudent.id);

        return student;
    }

    async updateStudentById({
        studentId,
        updateStudentByIdRequestDto,
    }: IUpdateStudentByIdParams): Promise<StudentResponseDto> {
        const { groupId, courseId } = updateStudentByIdRequestDto;

        const studentToUpdate: Student | null =
            await this.studentsRepository.findOneBy({
                id: studentId,
            });

        if (!studentToUpdate)
            throw new NotFoundException(HttpMessagesEnum.STUDENT_NOT_FOUND);

        const foundGroup: Group | null = await this.groupsRepository.findOneBy({
            id: groupId,
        });

        if (groupId && !foundGroup)
            throw new NotFoundException(HttpMessagesEnum.GROUP_NOT_FOUND);

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({
                id: courseId,
            });

        if (courseId && !foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { courseId: _, ...studentEntityUpdateBody } =
            updateStudentByIdRequestDto;

        await this.studentsRepository.update(
            studentId,
            studentEntityUpdateBody,
        );

        if (courseId && foundCourse)
            await this.studentCourseRepository.save({
                courseId,
                studentId,
            });

        const student = this.findStudentById(studentId);

        return student;
    }

    async deleteStudentById(studentId: string): Promise<void> {
        const foundStudent: Student | null =
            await this.studentsRepository.findOneBy({
                id: studentId,
            });

        if (!foundStudent)
            throw new NotFoundException(HttpMessagesEnum.STUDENT_NOT_FOUND);

        const { imagePath } = foundStudent;

        if (imagePath) {
            const imagePublicId = this.getImagePublicId(imagePath);

            await this.cloudinaryService.deleteImage(imagePublicId);
        }

        await this.studentsRepository.delete(studentId);
    }

    async deleteCourseFromStudentById({
        studentId,
        courseId,
    }: IDeleteCourseFromStudentByIdParams): Promise<void> {
        const studentCourse = await this.studentCourseRepository.findOneBy({
            studentId,
            courseId,
        });

        if (!studentCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        await this.studentCourseRepository.remove(studentCourse);
    }

    async addImageToStudentById({
        studentId,
        file,
    }: IAddImageToStudentByIdParams): Promise<StudentResponseDto> {
        const studentToUpdate: Student | null =
            await this.studentsRepository.findOneBy({
                id: studentId,
            });

        if (!studentToUpdate)
            throw new NotFoundException(HttpMessagesEnum.STUDENT_NOT_FOUND);

        if (!file)
            throw new BadRequestException(HttpMessagesEnum.FILE_NOT_PROVIDED);

        const uploadedImage = await this.cloudinaryService.uploadFile(file);

        const { imagePath } = studentToUpdate;

        if (imagePath) {
            const imagePublicId = this.getImagePublicId(imagePath);

            await this.cloudinaryService.deleteImage(imagePublicId);
        }

        await this.studentsRepository.update(studentId, {
            imagePath: uploadedImage.url,
        });

        const student = this.findStudentById(studentId);

        return student;
    }
}
