import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthGuard } from '../auth/guards/auth.guard';
import { CloudinaryService } from '../cloudinary/cloudinary.service';
import { Course } from '../courses/entities/course.entity';
import { Group } from '../groups/entities/group.entity';
import { Student } from './entitites/student.entity';
import { StudentCourse } from './entitites/student-course.entity';
import { StudentsController } from './students.controller';
import { StudentsService } from './students.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Student, Group, Course, StudentCourse]),
    ],
    controllers: [StudentsController],
    providers: [StudentsService, CloudinaryService, AuthGuard, JwtService],
})
export class StudentsModule {}
