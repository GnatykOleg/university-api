import { UpdateStudentByIdRequestDto } from '../dto/update-student-by-id.request.dto';

export interface IUpdateStudentByIdParams {
    studentId: string;
    updateStudentByIdRequestDto: UpdateStudentByIdRequestDto;
}
