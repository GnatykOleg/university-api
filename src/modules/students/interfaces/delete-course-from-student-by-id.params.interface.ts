export interface IDeleteCourseFromStudentByIdParams {
    studentId: string;
    courseId: string;
}
