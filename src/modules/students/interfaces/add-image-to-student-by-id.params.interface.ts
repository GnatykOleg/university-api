export interface IAddImageToStudentByIdParams {
    studentId: string;
    file: Express.Multer.File;
}
