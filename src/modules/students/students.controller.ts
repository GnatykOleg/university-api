import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UploadedFile,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiBody,
    ApiConflictResponse,
    ApiConsumes,
    ApiCreatedResponse,
    ApiExtraModels,
    ApiInternalServerErrorResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiOperation,
    ApiParam,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import ENDPOINTS from '../app/constants/endpoints.constant';
import { ConflictErrorResponseDto } from '../app/dto/errors/conflict-error.response.dto';
import { InternalServerErrorResponseDto } from '../app/dto/errors/internal-server-error.response.dto';
import { NotFoundErrorResponseDto } from '../app/dto/errors/not-found-error.response.dto';
import { UnauthorizeErrordResponseDto } from '../app/dto/errors/unauthorized-error.response.dto';
import { ValidationFailedErrorResponseDto } from '../app/dto/errors/validation-failed-error.response.dto';
import { FileUploadDto } from '../app/dto/file-upload.dto';
import { IdParamDto } from '../app/dto/params/id-param.dto';
import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateApiParamOptionsForId } from '../app/utils';
import { AuthGuard } from '../auth/guards/auth.guard';
import { CreateStudentRequestDto } from './dto/create-student.request.dto';
import { DeleteCourseFromStudentRequestDto } from './dto/delete-course-from-student.request.dto';
import { FindAllStudentsQueryDto } from './dto/find-all-students.query.dto';
import { FindAllStudentsResponseDto } from './dto/find-all-students.response.dto';
import { StudentResponseDto } from './dto/student.response.dto';
import { UpdateStudentByIdRequestDto } from './dto/update-student-by-id.request.dto';
import { StudentsService } from './students.service';

@UseGuards(AuthGuard)
@ApiBearerAuth('access-token')
@ApiTags('Students')
@ApiExtraModels(StudentResponseDto)
@Controller(ENDPOINTS.STUDENTS)
export class StudentsController {
    constructor(private readonly studentsService: StudentsService) {}

    // Find All Students
    @ApiOperation({ summary: 'Find all students with filter query' })
    @ApiOkResponse({
        description: 'Receives either an array of students, or an empty array',
        type: FindAllStudentsResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get()
    async findAllStudents(
        @Query() query: FindAllStudentsQueryDto,
    ): Promise<FindAllStudentsResponseDto> {
        const allStudents = await this.studentsService.findAllStudents(query);

        return allStudents;
    }

    // Find Student By Id
    @ApiOperation({ summary: 'Find student by id' })
    @ApiParam(generateApiParamOptionsForId('student', 'studentId'))
    @ApiOkResponse({
        description: 'The student has been successfully found',
        type: StudentResponseDto,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.STUDENT_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get(':id')
    async findStudentById(
        @Param() params: IdParamDto,
    ): Promise<StudentResponseDto> {
        const studentId = params.id;

        const foundStudent = await this.studentsService.findStudentById(
            studentId,
        );

        return foundStudent;
    }

    // Create Student
    @ApiOperation({ summary: 'Create student' })
    @ApiCreatedResponse({
        description: 'The student has been successfully created.',
        type: StudentResponseDto,
    })
    @ApiConflictResponse({
        description: HttpMessagesEnum.STUDENT_ALREDY_EXIST,
        type: ConflictErrorResponseDto,
    })
    @ApiNotFoundResponse({
        description: `${HttpMessagesEnum.GROUP_NOT_FOUND} / ${HttpMessagesEnum.COURSE_NOT_FOUND}`,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Post()
    async createStudent(
        @Body() createStudentRequestDto: CreateStudentRequestDto,
    ): Promise<StudentResponseDto> {
        const createdStudent = await this.studentsService.createStudent(
            createStudentRequestDto,
        );

        return createdStudent;
    }

    // Update Student By Id
    @ApiOperation({ summary: 'Update student by id' })
    @ApiBody({ required: false, type: UpdateStudentByIdRequestDto })
    @ApiParam(generateApiParamOptionsForId('student', 'studentId'))
    @ApiOkResponse({
        description: 'The student has been successfully updated',
        type: StudentResponseDto,
    })
    @ApiNotFoundResponse({
        description: `${HttpMessagesEnum.STUDENT_NOT_FOUND} / ${HttpMessagesEnum.GROUP_NOT_FOUND} / ${HttpMessagesEnum.COURSE_NOT_FOUND}`,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Patch(':id')
    async updateStudentById(
        @Param() params: IdParamDto,
        @Body() updateStudentByIdRequestDto: UpdateStudentByIdRequestDto,
    ): Promise<StudentResponseDto> {
        const studentId = params.id;

        const updatedStudent = await this.studentsService.updateStudentById({
            studentId,
            updateStudentByIdRequestDto,
        });

        return updatedStudent;
    }

    // Delete Student By Id
    @ApiOperation({ summary: 'Delete student by id' })
    @ApiParam(generateApiParamOptionsForId('student', 'studentId'))
    @ApiNoContentResponse({
        description: 'The student has been successfully deleted',
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.STUDENT_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteStudentById(@Param() params: IdParamDto): Promise<void> {
        const studentId = params.id;

        await this.studentsService.deleteStudentById(studentId);
    }

    // Delete Course From Student By Id
    @ApiOperation({ summary: 'Delete course from student by id' })
    @ApiParam(generateApiParamOptionsForId('course', 'studentId'))
    @ApiNoContentResponse({
        description: 'The student course has been successfully deleted',
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.COURSE_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Delete(':id/course')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteCourseFromStudentById(
        @Param() params: IdParamDto,
        @Body()
        deleteCourseFromStudentRequestDto: DeleteCourseFromStudentRequestDto,
    ): Promise<void> {
        const studentId = params.id;
        const courseId = deleteCourseFromStudentRequestDto.courseId;

        await this.studentsService.deleteCourseFromStudentById({
            studentId,
            courseId,
        });
    }

    // Add Image To Student By Id
    @ApiOperation({ summary: 'Add image to student by id' })
    @ApiParam(generateApiParamOptionsForId('student', 'studentId'))
    @ApiOkResponse({
        description: 'The student image has been successfully added',
        type: StudentResponseDto,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.STUDENT_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: `${HttpMessagesEnum.VALIDATION_FAILED} / ${HttpMessagesEnum.FILE_NOT_PROVIDED}`,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Patch(':id/image')
    @UseInterceptors(FileInterceptor('file'))
    @ApiConsumes('multipart/form-data')
    @ApiBody({
        description: 'Upload file',
        type: FileUploadDto,
    })
    async addImageToStudentById(
        @Param() params: IdParamDto,
        @UploadedFile() file: Express.Multer.File,
    ): Promise<StudentResponseDto> {
        const studentId = params.id;

        const studentWithImage =
            await this.studentsService.addImageToStudentById({
                studentId,
                file,
            });

        return studentWithImage;
    }
}
