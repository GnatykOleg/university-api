import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

import { Course } from '../../courses/entities/course.entity';
import { Student } from './student.entity';

@Entity('student_course')
export class StudentCourse {
    @PrimaryColumn({ name: 'student_id' })
    studentId: string;

    @ManyToOne(() => Student, (student) => student.courses)
    @JoinColumn([{ name: 'student_id', referencedColumnName: 'id' }])
    students: Student[];

    @PrimaryColumn({ name: 'course_id' })
    courseId: string;

    @ManyToOne(() => Course, (course) => course.students)
    @JoinColumn([{ name: 'course_id', referencedColumnName: 'id' }])
    courses: Course[];
}
