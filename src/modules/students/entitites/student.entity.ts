import { ApiProperty } from '@nestjs/swagger';
import {
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
} from 'typeorm';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
    @ApiProperty(apiPropertyOptions.PERSON_NAME)
    @Column({ type: 'varchar', nullable: false, length: 50 })
    @Index()
    name: string;

    @ApiProperty(apiPropertyOptions.PERSON_SURNAME)
    @Column({ type: 'varchar', nullable: false, length: 50 })
    @Index()
    surname: string;

    @ApiProperty(apiPropertyOptions.PERSON_EMAIL)
    @Column({ type: 'varchar', nullable: false, length: 50 })
    email: string;

    @ApiProperty(apiPropertyOptions.PERSON_AGE)
    @Column({ type: 'integer', nullable: false })
    age: number;

    @ApiProperty(apiPropertyOptions.PERSON_IMAGE_PATH_NULLABLE)
    @Column({ name: 'image_path', nullable: true, length: 350 })
    imagePath: string;

    @OneToMany(() => Mark, (mark) => mark.student, {
        cascade: ['remove'],
    })
    marks: Mark[];

    @ApiProperty(apiPropertyOptions.GROUP_ID)
    @Column({
        type: 'uuid',
        nullable: true,
        name: 'group_id',
    })
    groupId: string;

    @ManyToOne(() => Group, (group) => group.students, {
        nullable: true,
        eager: false,
        onDelete: 'SET NULL',
    })
    @JoinColumn({ name: 'group_id' })
    group: Group;

    @ManyToMany(() => Course, (course) => course.students, {
        onDelete: 'CASCADE',
        onUpdate: 'NO ACTION',
    })
    @JoinTable({
        name: 'student_course',
        joinColumn: {
            name: 'student_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'course_id',
            referencedColumnName: 'id',
        },
    })
    courses: Course[];
}
