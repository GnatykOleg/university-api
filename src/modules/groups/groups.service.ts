import {
    ConflictException,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateLikeFindOperator } from '../app/utils';
import { CreateGroupRequestDto } from './dto/create-group.request.dto';
import { FindAllGroupsQueryDto } from './dto/find-all-groups.query.dto';
import { FindAllGroupsResponseDto } from './dto/find-all-groups.response.dto';
import { Group } from './entities/group.entity';
import { IUpdateGroupByIdParams } from './interfaces/update-group-by-id.params.interface';

@Injectable()
export class GroupsService {
    constructor(
        @InjectRepository(Group)
        private readonly groupsRepository: Repository<Group>,
    ) {}

    async findAllGroups(
        query: FindAllGroupsQueryDto,
    ): Promise<FindAllGroupsResponseDto> {
        const { sort_field, sort_order, name, page, per_page } = query;

        const [data, total] = await this.groupsRepository.findAndCount({
            where: { name: generateLikeFindOperator(name) },

            order: { [sort_field]: sort_order },

            skip: per_page * (page - 1),
            take: per_page,
        });

        return { data, total };
    }

    async findGroupById(groupId: string): Promise<Group> {
        const foundGroup: Group | null = await this.groupsRepository.findOneBy({
            id: groupId,
        });

        if (!foundGroup)
            throw new NotFoundException(HttpMessagesEnum.GROUP_NOT_FOUND);

        return foundGroup;
    }

    async createGroup(
        createGrouptRequestDto: CreateGroupRequestDto,
    ): Promise<Group> {
        const { name } = createGrouptRequestDto;

        const foundGroup: Group | null = await this.groupsRepository.findOneBy({
            name,
        });

        if (foundGroup)
            throw new ConflictException(HttpMessagesEnum.GROUP_ALREDY_EXIST);

        const createdGroup = this.groupsRepository.save(createGrouptRequestDto);

        return createdGroup;
    }

    async updateGroupById({
        groupId,
        updateGroupByIdRequestDto,
    }: IUpdateGroupByIdParams): Promise<Group> {
        const updatedGroup: UpdateResult = await this.groupsRepository.update(
            groupId,
            updateGroupByIdRequestDto,
        );

        if (!updatedGroup.affected)
            throw new NotFoundException(HttpMessagesEnum.GROUP_NOT_FOUND);

        const foundGroup = this.groupsRepository.findOneBy({
            id: groupId,
        });

        return foundGroup;
    }

    async deleteGroupById(groupId: string): Promise<void> {
        const deletedGroup: DeleteResult = await this.groupsRepository.delete(
            groupId,
        );

        if (!deletedGroup.affected)
            throw new NotFoundException(HttpMessagesEnum.GROUP_NOT_FOUND);
    }
}
