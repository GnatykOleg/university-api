import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, Index, OneToMany } from 'typeorm';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Student } from '../../students/entitites/student.entity';

@Entity({ name: 'groups' })
export class Group extends CoreEntity {
    @ApiProperty(apiPropertyOptions.GROUP_NAME)
    @Column({ type: 'varchar', nullable: false, length: 50 })
    @Index()
    name: string;

    @OneToMany(() => Student, (student) => student.group, {
        cascade: ['remove'],
    })
    students: Student[];
}
