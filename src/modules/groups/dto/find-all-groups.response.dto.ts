import { ApiProperty } from '@nestjs/swagger';

import { Group } from '../entities/group.entity';

export class FindAllGroupsResponseDto {
    @ApiProperty({
        type: [Group],
        description: 'Array of Group objects or an empty array',
    })
    data: Group[] | [];

    @ApiProperty({ default: 1 })
    total: number;
}
