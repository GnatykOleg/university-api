import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { MaxLength, MinLength } from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CLASS_VALIDATOR_VALUES from '../../app/constants/class-validator-values.constant';
import { lowerCaseAndTrim } from '../../app/utils';

export class CreateGroupRequestDto {
    @ApiProperty(apiPropertyOptions.GROUP_NAME)
    @Transform(lowerCaseAndTrim)
    @MinLength(CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH)
    @MaxLength(CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH)
    name: string;
}
