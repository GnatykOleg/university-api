import { UpdateGroupByIdRequestDto } from '../dto/update-group-by-id.request.dto';

export interface IUpdateGroupByIdParams {
    groupId: string;
    updateGroupByIdRequestDto: UpdateGroupByIdRequestDto;
}
