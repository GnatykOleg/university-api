import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UseGuards,
} from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiBody,
    ApiConflictResponse,
    ApiCreatedResponse,
    ApiInternalServerErrorResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiOperation,
    ApiParam,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import ENDPOINTS from '../app/constants/endpoints.constant';
import { ConflictErrorResponseDto } from '../app/dto/errors/conflict-error.response.dto';
import { InternalServerErrorResponseDto } from '../app/dto/errors/internal-server-error.response.dto';
import { NotFoundErrorResponseDto } from '../app/dto/errors/not-found-error.response.dto';
import { UnauthorizeErrordResponseDto } from '../app/dto/errors/unauthorized-error.response.dto';
import { ValidationFailedErrorResponseDto } from '../app/dto/errors/validation-failed-error.response.dto';
import { IdParamDto } from '../app/dto/params/id-param.dto';
import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateApiParamOptionsForId } from '../app/utils';
import { AuthGuard } from '../auth/guards/auth.guard';
import { CreateGroupRequestDto } from './dto/create-group.request.dto';
import { FindAllGroupsQueryDto } from './dto/find-all-groups.query.dto';
import { FindAllGroupsResponseDto } from './dto/find-all-groups.response.dto';
import { UpdateGroupByIdRequestDto } from './dto/update-group-by-id.request.dto';
import { Group } from './entities/group.entity';
import { GroupsService } from './groups.service';

@UseGuards(AuthGuard)
@ApiBearerAuth('access-token')
@ApiTags('Groups')
@Controller(ENDPOINTS.GROUPS)
export class GroupsController {
    constructor(private readonly groupsService: GroupsService) {}

    // Find All groups
    @ApiOperation({ summary: 'Find all groups with filter query' })
    @ApiOkResponse({
        description: 'Receives an array of groups, or an empty array',
        type: FindAllGroupsResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get()
    async findAllGroups(
        @Query() query: FindAllGroupsQueryDto,
    ): Promise<FindAllGroupsResponseDto> {
        const allGroups = await this.groupsService.findAllGroups(query);

        return allGroups;
    }

    // Find Group By Id
    @ApiOperation({ summary: 'Find group by id' })
    @ApiParam(generateApiParamOptionsForId('group', 'groupId'))
    @ApiOkResponse({
        description: 'The group has been successfully found',
        type: Group,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.GROUP_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get(':id')
    async findGroupById(@Param() params: IdParamDto): Promise<Group> {
        const groupId = params.id;

        const foundGroupWithStudents = await this.groupsService.findGroupById(
            groupId,
        );

        return foundGroupWithStudents;
    }

    // Create Group
    @ApiOperation({ summary: 'Create group' })
    @ApiCreatedResponse({
        description: 'The group has been successfully created',
        type: Group,
    })
    @ApiConflictResponse({
        description: HttpMessagesEnum.GROUP_ALREDY_EXIST,
        type: ConflictErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Post()
    async createGroup(
        @Body() createGrouptRequestDto: CreateGroupRequestDto,
    ): Promise<Group> {
        const createdGroup = await this.groupsService.createGroup(
            createGrouptRequestDto,
        );

        return createdGroup;
    }

    // Update Group By Id
    @ApiOperation({ summary: 'Update group by id' })
    @ApiBody({ required: false, type: UpdateGroupByIdRequestDto })
    @ApiParam(generateApiParamOptionsForId('group', 'groupId'))
    @ApiOkResponse({
        description: 'The group has been successfully updated',
        type: Group,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.GROUP_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Patch(':id')
    async updateGroupById(
        @Param() params: IdParamDto,
        @Body() updateGroupByIdRequestDto: UpdateGroupByIdRequestDto,
    ): Promise<Group> {
        const groupId = params.id;

        const updatedGroup = await this.groupsService.updateGroupById({
            groupId,
            updateGroupByIdRequestDto,
        });

        return updatedGroup;
    }

    // Delete group By Id
    @ApiOperation({ summary: 'Delete group by id' })
    @ApiParam(generateApiParamOptionsForId('group', 'groupId'))
    @ApiNoContentResponse({
        description: 'The group has been successfully deleted',
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.GROUP_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteGroupById(@Param() params: IdParamDto): Promise<void> {
        const groupId = params.id;

        await this.groupsService.deleteGroupById(groupId);
    }
}
