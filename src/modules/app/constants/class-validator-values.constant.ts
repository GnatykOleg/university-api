const CLASS_VALIDATOR_VALUES = Object.freeze({
    VARCHAR_MIN_LENGTH: 3,
    VARCHAR_MAX_LENGTH: 50,

    TEXT_MIN_LENGTH: 10,

    INT_AGE_MIN: 18,
    INT_AGE_MAX: 150,

    INT_HOURS_MIN: 100,
    INT_HOURS_MAX: 1000,

    INT_MARK_MIN: 1,
    INT_MARK_MAX: 10,

    IS_STRONG_PASSWORD_OPTIONS: {
        minLength: 5,
        minUppercase: 1,
        minSymbols: 1,
        minNumbers: 1,
    },

    IS_STRONG_PASSWORD_OPTIONS_MESSAGE:
        'The password should be at least 5 characters long, contain at least 1 uppercase letter, 1 symbol, and 1 number.',
});

export default CLASS_VALIDATOR_VALUES;
