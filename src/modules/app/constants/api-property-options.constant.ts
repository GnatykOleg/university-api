import { ApiPropertyOptions } from '@nestjs/swagger';

import CLASS_VALIDATOR_VALUES from './class-validator-values.constant';

// COMMON

export const COMMON_ID: ApiPropertyOptions = {
    format: 'uuid',
    example: '9e760f74-3c8b-40ba-bfc7-422be78589e5',
};

export const STUDENT_ID: ApiPropertyOptions = {
    format: 'uuid',
    example: '6e90e850-80af-47b2-97a0-fcf04146fe76',
};

export const LECTOR_ID: ApiPropertyOptions = {
    format: 'uuid',
    example: '90ae10d6-a47a-474e-abe1-7551f754ed9c',
};

export const GROUP_ID: ApiPropertyOptions = {
    format: 'uuid',
    example: '02877cc6-1a76-4cc1-95f2-b18721b9a42b',
};

export const COURSE_ID: ApiPropertyOptions = {
    format: 'uuid',
    example: 'd309c5bc-21ee-433c-9562-2ff8a3ec96f6',
};

export const CREATED_AT: ApiPropertyOptions = {
    format: 'date-time',
    example: '2023-08-24T13:54:59.425Z',
};

export const UPDATED_AT: ApiPropertyOptions = {
    format: 'date-time',
    example: '2023-08-24T13:54:59.425Z',
};

// AUTH

export const JWT_TOKEN: ApiPropertyOptions = {
    format: 'JWT-token',
    example:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3OGY0MjI5NC02N2EzLTQxNWYtYWRiYi02NTZkMTVkY2Y1OWUiLCJlbWFpbCI6Im9sZWdAbWFpbC5jb20iLCJpYXQiOjE2OTMxMzYyMjUsImV4cCI6MTY5MzE4NjIyNX0.ofa9SUhfFJLVu3-1LgTUG0FbFo_pkVDiGs0DYqldqGk',
};

// PERSONS
export const PERSON_NAME: ApiPropertyOptions = {
    example: 'john',
    minLength: CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH,
    maxLength: CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH,
};

export const PERSON_SURNAME: ApiPropertyOptions = {
    example: 'smith',
    minLength: CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH,
    maxLength: CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH,
};

export const PERSON_NAME_NULLABLE: ApiPropertyOptions = {
    ...PERSON_NAME,
    nullable: true,
};

export const PERSON_SURNAME_NULLABLE: ApiPropertyOptions = {
    ...PERSON_SURNAME,
    nullable: true,
};

export const PERSON_PASSWORD: ApiPropertyOptions = {
    format: 'password',
    example: 'John=4',
    description: CLASS_VALIDATOR_VALUES.IS_STRONG_PASSWORD_OPTIONS_MESSAGE,
};

export const PERSON_EMAIL: ApiPropertyOptions = {
    format: 'email',
    example: 'johnsmith@mail.com',
    minLength: CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH,
    maxLength: CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH,
};

export const PERSON_AGE: ApiPropertyOptions = {
    format: 'integer',
    example: 20,
    minimum: CLASS_VALIDATOR_VALUES.INT_AGE_MIN,
    maximum: CLASS_VALIDATOR_VALUES.INT_AGE_MAX,
};

export const PERSON_GROUP_NAME: ApiPropertyOptions = {
    example: 'bc-22',
};

export const PERSON_COURSE_ID_NULLABLE: ApiPropertyOptions = {
    ...COURSE_ID,
    nullable: true,
};

export const PERSON_IMAGE_PATH_NULLABLE: ApiPropertyOptions = {
    example:
        'http://res.cloudinary.com/dihwu7ikh/image/upload/v1692878116/university-api/students/file_qkt1d3.jpg',
    nullable: true,
};

// GROUP

export const GROUP_NAME: ApiPropertyOptions = {
    example: 'bc-22',
    minLength: CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH,
    maxLength: CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH,
};

// MARK

export const MARK: ApiPropertyOptions = {
    format: 'integer',
    example: 8,
    minimum: CLASS_VALIDATOR_VALUES.INT_MARK_MIN,
    maximum: CLASS_VALIDATOR_VALUES.INT_MARK_MAX,
};

// COURSE

export const COURSE_NAME: ApiPropertyOptions = {
    example: 'astronomy',
    minLength: CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH,
    maxLength: CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH,
};

export const COURSE_DESCRIPTION: ApiPropertyOptions = {
    example:
        "The most captivating course in astronomy, let's explore the depths of space.",
    minLength: CLASS_VALIDATOR_VALUES.TEXT_MIN_LENGTH,
};

export const COURSE_HOURS: ApiPropertyOptions = {
    format: 'integer',
    example: 500,
    minimum: CLASS_VALIDATOR_VALUES.INT_HOURS_MIN,
    maximum: CLASS_VALIDATOR_VALUES.INT_HOURS_MAX,
};
