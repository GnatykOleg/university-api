const ENDPOINTS = Object.freeze({
    STUDENTS: 'api/v1/students',
    GROUPS: 'api/v1/groups',
    LECTORS: 'api/v1/lectors',
    COURSES: 'api/v1/courses',
    MARKS: 'api/v1/marks',

    AUTH: 'api/v1/auth',

    SWAGGER: 'api/v1/docs',
});

export default ENDPOINTS;
