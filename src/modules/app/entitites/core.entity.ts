import { ApiProperty } from '@nestjs/swagger';
import {
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import * as apiPropertyOptions from '../constants/api-property-options.constant';

abstract class CoreEntity extends BaseEntity {
    @ApiProperty(apiPropertyOptions.COMMON_ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @ApiProperty(apiPropertyOptions.CREATED_AT)
    @CreateDateColumn({ type: 'timestamp with time zone', name: 'created_at' })
    public createdAt: Date;

    @ApiProperty(apiPropertyOptions.UPDATED_AT)
    @UpdateDateColumn({ type: 'timestamp with time zone', name: 'updated_at' })
    public updatedAt: Date;
}

export default CoreEntity;
