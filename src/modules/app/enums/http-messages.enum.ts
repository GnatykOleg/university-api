export enum HttpMessagesEnum {
    STUDENT_NOT_FOUND = 'We did not find such a student',
    STUDENT_ALREDY_EXIST = 'Such a student already exists',

    GROUP_NOT_FOUND = 'We did not find such a group',
    GROUP_ALREDY_EXIST = 'Such a group already exists',

    LECTOR_NOT_FOUND = 'We did not find such a lector',
    LECTOR_ALREDY_EXIST = 'Such a lector already exists',

    COURSE_NOT_FOUND = 'We did not find such a course/courses',
    COURSE_ALREDY_EXIST = 'Such a course already exists',

    MARK_NOT_FOUND = 'We did not find such a mark/marks',

    FILE_NOT_PROVIDED = 'File is not provided',

    ISE = 'Internal server error',

    VALIDATION_FAILED = 'Validation failed',

    AUTH_FAILED = 'Auth failed',

    UNAUTHORIZED = 'Unauthorized: You do not have permission to access this resource',

    WRONG_SIGN_IN_DATA = 'Incorrect password or email',
    WRONG_RESET_TOKEN = 'Invalid or expired reset token',

    USER_ALREDY_EXIST = 'User already exists',
    USER_NOT_FOUND = 'User not found',
}
