import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import appConfig from '../../configs/app/app.config';
import authConfig from '../../configs/auth/auth.config';
import mailConfig from '../../configs/auth/mail.config';
import cloudinaryConfig from '../../configs/cloudinary/cloudinary.config';
import databaseConfig from '../../configs/database/database.config';
import typeOrmAsyncFactoryConfig from '../../configs/database/typeorm-async-factory.config';
import { AuthModule } from '../auth/auth.module';
import { CloudinaryModule } from '../cloudinary/cloudinary.module';
import { CoursesModule } from '../courses/courses.module';
import { GroupsModule } from '../groups/groups.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MailModule } from '../mail/mail.module';
import { MarksModule } from '../marks/marks.module';
import { ResetTokenModule } from '../reset-token/reset-token.module';
import { StudentsModule } from '../students/students.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            cache: true,
            isGlobal: true,
            load: [
                appConfig,
                databaseConfig,
                cloudinaryConfig,
                authConfig,
                mailConfig,
            ],
        }),

        TypeOrmModule.forRootAsync(typeOrmAsyncFactoryConfig),

        MailModule,

        StudentsModule,
        GroupsModule,
        LectorsModule,
        CoursesModule,
        MarksModule,
        CloudinaryModule,
        ResetTokenModule,
        AuthModule,
    ],
})
export class AppModule {}
