import { ApiProperty } from '@nestjs/swagger';

export class NotFoundErrorResponseDto {
    @ApiProperty({ description: 'HTTP status code', example: 404 })
    statusCode: number;

    @ApiProperty({
        example: 'Some resource not found',
        description: 'Error message',
    })
    message: string;

    @ApiProperty({
        example: 'Not Found',
        description: 'Some error',
    })
    error: string;
}
