import { ApiProperty } from '@nestjs/swagger';

export class ValidationFailedErrorResponseDto {
    @ApiProperty({ description: 'HTTP status code', example: 400 })
    statusCode: number;

    @ApiProperty({
        example: ['some value must be a string', 'some value must be a UUID'],
        description: 'Array of error messages',
    })
    message: string[];

    @ApiProperty({
        example: 'Bad Request',
        description: 'Some error',
    })
    error: string;
}
