import { ApiProperty } from '@nestjs/swagger';

import { HttpMessagesEnum } from '../../enums/http-messages.enum';

export class UnauthorizeErrordResponseDto {
    @ApiProperty({ description: 'HTTP status code', example: 401 })
    statusCode: number;

    @ApiProperty({
        example: HttpMessagesEnum.UNAUTHORIZED,
        description: 'Error message',
    })
    message: string;

    @ApiProperty({
        example: 'Unauthorized',
        description: 'Some error',
    })
    error: string;
}
