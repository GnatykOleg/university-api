import { ApiProperty } from '@nestjs/swagger';

import { HttpMessagesEnum } from '../../enums/http-messages.enum';

export class InternalServerErrorResponseDto {
    @ApiProperty({ description: 'HTTP status code', example: 500 })
    statusCode: number;

    @ApiProperty({
        example: HttpMessagesEnum.ISE,
        description: 'Error message',
    })
    message: string;
}
