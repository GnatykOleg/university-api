import { ApiProperty } from '@nestjs/swagger';

export class ConflictErrorResponseDto {
    @ApiProperty({ description: 'HTTP status code', example: 409 })
    statusCode: number;

    @ApiProperty({
        example: 'Some resource already exists',
        description: 'Error message',
    })
    message: string;

    @ApiProperty({
        example: 'Conflict',
        description: 'Some error',
    })
    error: string;
}
