import { BadRequestException } from '@nestjs/common';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsIn, IsInt, IsOptional, IsString, Max, Min } from 'class-validator';

const errorMessage =
    "Query param {sort_order}: ('ASC' | 'DESC') and {sort_filed}: (use valid fields createdAt | updatedAt | name | age etc) must be used together";

export class SortPaginationQueryDto {
    @ApiPropertyOptional({ description: errorMessage })
    @Transform(({ value, obj }) => {
        if ((value && !obj.sort_order) || (!value && obj.sort_order))
            throw new BadRequestException(errorMessage);

        return value;
    })
    @IsOptional()
    @IsString()
    sort_field?: string | undefined;

    @ApiPropertyOptional({
        description: errorMessage,
    })
    @Transform(({ value, obj }) => {
        if ((value && !obj.sort_field) || (!value && obj.sort_field))
            throw new BadRequestException(errorMessage);

        return value;
    })
    @IsOptional()
    @IsString()
    @IsIn(['ASC', 'DESC'])
    sort_order?: 'ASC' | 'DESC' | undefined;

    @ApiPropertyOptional({
        description: 'min:1 | max: none | default: 1',
    })
    @Type(() => Number)
    @IsInt()
    @Min(1)
    @IsOptional()
    page?: number = 1;

    @ApiPropertyOptional({
        description: 'min: 1 | max: 50 | default: 10',
    })
    @Type(() => Number)
    @IsInt()
    @Min(1)
    @Max(50)
    @IsOptional()
    per_page?: number = 10;
}
