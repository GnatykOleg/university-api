import { NextFunction, Request, Response } from 'express';

function logger(req: Request, _: Response, next: NextFunction): void {
    const { url, method } = req;

    console.log(`URL: ${url} | Method: ${method}`);

    next();
}

export default logger;
