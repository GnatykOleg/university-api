import { BadRequestException } from '@nestjs/common';

const lowerCaseAndTrim = <T>({ value }: { value: T }): string => {
    if (typeof value !== 'string')
        throw new BadRequestException('value must be a string');

    return value.toLowerCase().trim();
};

export default lowerCaseAndTrim;
