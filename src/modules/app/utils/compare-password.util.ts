import bcrypt from 'bcrypt';

const comparePassword = async (
    rawPassword: string,
    hash: string,
): Promise<boolean> => bcrypt.compare(rawPassword, hash);

export default comparePassword;
