import { ApiParamOptions } from '@nestjs/swagger';

const generateApiParamOptionsForId = (
    target: string,
    whatResourceId:
        | 'groupId'
        | 'studentId'
        | 'lectorId'
        | 'courseId'
        | 'markId',
): ApiParamOptions => ({
    name: 'id',
    type: 'string',
    format: 'uuid',
    example: 'd5935803-7e10-429f-a8f0-7912ac765d86',
    description: `Parameter for searching a ${target} by ${whatResourceId}`,
});

export default generateApiParamOptionsForId;
