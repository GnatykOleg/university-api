import { FindOperator, Like } from 'typeorm';

const generateLikeFindOperator = (value: string): FindOperator<string> =>
    value && Like(`${value}%`);

export default generateLikeFindOperator;
