import bcrypt from 'bcrypt';

const encodePassword = async (rawPassword: string): Promise<string> => {
    const SALT = await bcrypt.genSalt();

    const encodedPassword = bcrypt.hash(rawPassword, SALT);

    return encodedPassword;
};

export default encodePassword;
