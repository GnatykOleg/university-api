import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import jwtAsyncFactoryConfig from '../../configs/auth/jwt-async-factory.config';
import { LectorsModule } from '../lectors/lectors.module';
import { MailModule } from '../mail/mail.module';
import { ResetTokenModule } from '../reset-token/reset-token.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
    imports: [
        LectorsModule,
        ResetTokenModule,
        JwtModule.registerAsync(jwtAsyncFactoryConfig),
        MailModule,
    ],
    providers: [AuthService],
    controllers: [AuthController],
    exports: [AuthService],
})
export class AuthModule {}
