import {
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Post,
    UseGuards,
} from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiConflictResponse,
    ApiInternalServerErrorResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiOperation,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import ENDPOINTS from '../app/constants/endpoints.constant';
import { ConflictErrorResponseDto } from '../app/dto/errors/conflict-error.response.dto';
import { InternalServerErrorResponseDto } from '../app/dto/errors/internal-server-error.response.dto';
import { NotFoundErrorResponseDto } from '../app/dto/errors/not-found-error.response.dto';
import { UnauthorizeErrordResponseDto } from '../app/dto/errors/unauthorized-error.response.dto';
import { ValidationFailedErrorResponseDto } from '../app/dto/errors/validation-failed-error.response.dto';
import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { Lector } from '../lectors/entities/lector.entity';
import { AuthService } from './auth.service';
import { CurrentUser } from './decorators/current.user.decorator';
import { AccessTokenResponseDto } from './dto/acces-token.response.dto';
import { ResetPasswordRequestDto } from './dto/reset-password.request.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/reset-password-with-token.request.dto';
import { SignRequestDto } from './dto/sign.request.dto';
import { AuthGuard } from './guards/auth.guard';
import { ICurrentUser } from './interfaces/current-user.interface';

@ApiTags('Auth')
@Controller(ENDPOINTS.AUTH)
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    // Get Current user
    @UseGuards(AuthGuard)
    @ApiBearerAuth('access-token')
    @ApiOperation({ summary: 'Get current user' })
    @ApiOkResponse({
        description: 'Get current user',
        type: Lector,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.USER_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @Get('/me')
    async findCurrentUser(
        @CurrentUser() loggedUser: ICurrentUser,
    ): Promise<Lector> {
        const foundUser = await this.authService.findCurrentUser(
            loggedUser.sub,
        );

        return foundUser;
    }

    // Sign Up
    @ApiOperation({ summary: 'Sign Up' })
    @ApiNoContentResponse({
        description: 'Successfully sign up',
    })
    @ApiConflictResponse({
        description: HttpMessagesEnum.USER_ALREDY_EXIST,
        type: ConflictErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Post('sign-up')
    async signUp(@Body() signUpDto: SignRequestDto): Promise<void> {
        await this.authService.signUp(signUpDto);
    }

    // Sign In
    @ApiOperation({ summary: 'Sign In' })
    @ApiOkResponse({
        description: 'Successfully sign in',
        type: AccessTokenResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.WRONG_SIGN_IN_DATA,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @HttpCode(HttpStatus.OK)
    @Post('sign-in')
    async signIn(
        @Body() signRequestDto: SignRequestDto,
    ): Promise<AccessTokenResponseDto> {
        const accessToken = await this.authService.signIn(signRequestDto);

        return accessToken;
    }

    // Reset Password Request
    @ApiOperation({ summary: 'Reset password request' })
    @ApiNoContentResponse({
        description: 'User get email with link to reset password',
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.USER_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Post('reset-password-request')
    async resetPasswordRequest(
        @Body() resetPasswordRequestDto: ResetPasswordRequestDto,
    ): Promise<void> {
        const { email } = resetPasswordRequestDto;

        await this.authService.resetPasswordRequest(email);
    }

    // Reset Password
    @ApiOperation({ summary: 'Reset password' })
    @ApiNoContentResponse({
        description: 'The password has been successfully changed',
    })
    @ApiBadRequestResponse({
        description: `${HttpMessagesEnum.WRONG_RESET_TOKEN} / ${HttpMessagesEnum.VALIDATION_FAILED}`,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.USER_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Post('reset-password')
    @HttpCode(HttpStatus.NO_CONTENT)
    async resetPassword(
        @Body()
        resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
    ): Promise<void> {
        await this.authService.resetPassword(resetPasswordWithTokenRequestDto);
    }
}
