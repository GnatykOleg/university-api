import {
    CanActivate,
    ExecutionContext,
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

import { HttpMessagesEnum } from '../../app/enums/http-messages.enum';

@Injectable()
export class AuthGuard implements CanActivate {
    private readonly jwtSecret: string;

    constructor(
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
    ) {
        this.jwtSecret = this.configService.get<string>('auth.jwtSecret');
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        const token = this.extractTokenFromHeader(request);

        if (!token)
            throw new UnauthorizedException(HttpMessagesEnum.UNAUTHORIZED);

        try {
            const payload = await this.jwtService.verifyAsync(token, {
                secret: this.jwtSecret,
            });

            request['user'] = payload;
        } catch {
            throw new UnauthorizedException(HttpMessagesEnum.UNAUTHORIZED);
        }
        return true;
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];

        return type === 'Bearer' ? token : undefined;
    }
}
