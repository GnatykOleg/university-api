import {
    BadRequestException,
    Injectable,
    NotFoundException,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { comparePassword } from '../app/utils';
import { Lector } from '../lectors/entities/lector.entity';
import { LectorsService } from '../lectors/lectors.service';
import { MailService } from '../mail/mail.service';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { AccessTokenResponseDto } from './dto/acces-token.response.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/reset-password-with-token.request.dto';
import { SignRequestDto } from './dto/sign.request.dto';

@Injectable()
export class AuthService {
    constructor(
        private readonly lectorsService: LectorsService,
        private readonly jwtService: JwtService,
        private readonly resetTokenService: ResetTokenService,
        private readonly mailService: MailService,
    ) {}

    private async createToken({
        id,
        email,
    }: {
        id: string;
        email: string;
    }): Promise<AccessTokenResponseDto> {
        const payload = { sub: id, email };

        const accessToken = {
            accessToken: await this.jwtService.signAsync(payload),
        };

        return accessToken;
    }

    async findCurrentUser(userId: string): Promise<Lector> {
        const user = await this.lectorsService.findLectorById(userId);

        delete user.password;

        if (!user) throw new NotFoundException(HttpMessagesEnum.USER_NOT_FOUND);

        return user;
    }

    async signUp({ email, password }: SignRequestDto): Promise<void> {
        const user = await this.lectorsService.findLectorByEmail(email);

        if (user)
            throw new BadRequestException(HttpMessagesEnum.USER_ALREDY_EXIST);

        await this.lectorsService.createLector({ email, password });
    }

    async signIn({
        email,
        password,
    }: SignRequestDto): Promise<AccessTokenResponseDto> {
        const user = await this.lectorsService.findLectorByEmail(email);

        if (!user)
            throw new UnauthorizedException(
                HttpMessagesEnum.WRONG_SIGN_IN_DATA,
            );

        const passwordCompares = await comparePassword(password, user.password);

        if (!passwordCompares)
            throw new UnauthorizedException(
                HttpMessagesEnum.WRONG_SIGN_IN_DATA,
            );

        const accessToken = this.createToken({ email, id: user.id });

        return accessToken;
    }

    async resetPasswordRequest(email: string): Promise<void> {
        const user = await this.lectorsService.findLectorByEmail(email);

        if (!user)
            throw new BadRequestException(HttpMessagesEnum.USER_NOT_FOUND);

        const foundResetToken =
            await this.resetTokenService.findResetTokenByUserId(user.id);

        if (foundResetToken)
            await this.resetTokenService.deleteResetTokenByUserId(user.id);

        const resetToken = await this.resetTokenService.generateResetToken(
            user.id,
        );

        const { name } = user;

        await this.mailService.sendUserResetPassowrdLink({
            resetToken,
            email,
            name,
        });
    }

    async resetPassword(
        resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
    ): Promise<void> {
        const { resetToken, newPassword } = resetPasswordWithTokenRequestDto;

        const foundResetToken = await this.resetTokenService.findResetToken(
            resetToken,
        );

        if (!foundResetToken)
            throw new BadRequestException(HttpMessagesEnum.WRONG_RESET_TOKEN);

        const updateLectorByIdRequestDto = { password: newPassword };

        await this.lectorsService.updateLectorById({
            lectorId: foundResetToken.userId,
            updateLectorByIdRequestDto,
        });

        await this.resetTokenService.deleteResetTokenByUserId(
            foundResetToken.userId,
        );
    }
}
