import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
    IsEmail,
    IsString,
    IsStrongPassword,
    MaxLength,
    MinLength,
} from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CLASS_VALIDATOR_VALUES from '../../app/constants/class-validator-values.constant';
import { lowerCaseAndTrim } from '../../app/utils';

export class SignRequestDto {
    @ApiProperty(apiPropertyOptions.PERSON_EMAIL)
    @Transform(lowerCaseAndTrim)
    @IsEmail()
    @MinLength(CLASS_VALIDATOR_VALUES.VARCHAR_MIN_LENGTH)
    @MaxLength(CLASS_VALIDATOR_VALUES.VARCHAR_MAX_LENGTH)
    email: string;

    @ApiProperty(apiPropertyOptions.PERSON_PASSWORD)
    @IsString()
    @IsStrongPassword(CLASS_VALIDATOR_VALUES.IS_STRONG_PASSWORD_OPTIONS, {
        message: CLASS_VALIDATOR_VALUES.IS_STRONG_PASSWORD_OPTIONS_MESSAGE,
    })
    password: string;
}
