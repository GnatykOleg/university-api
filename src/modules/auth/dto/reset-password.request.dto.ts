import { OmitType } from '@nestjs/swagger';

import { SignRequestDto } from './sign.request.dto';

export class ResetPasswordRequestDto extends OmitType(SignRequestDto, [
    'password',
]) {}
