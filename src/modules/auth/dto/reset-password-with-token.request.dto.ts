import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsStrongPassword } from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CLASS_VALIDATOR_VALUES from '../../app/constants/class-validator-values.constant';

export class ResetPasswordWithTokenRequestDto {
    @ApiProperty(apiPropertyOptions.JWT_TOKEN)
    @IsNotEmpty()
    @IsString()
    @ApiProperty(apiPropertyOptions.JWT_TOKEN)
    resetToken: string;

    @ApiProperty(apiPropertyOptions.PERSON_PASSWORD)
    @IsString()
    @IsStrongPassword(CLASS_VALIDATOR_VALUES.IS_STRONG_PASSWORD_OPTIONS, {
        message: CLASS_VALIDATOR_VALUES.IS_STRONG_PASSWORD_OPTIONS_MESSAGE,
    })
    newPassword: string;
}
