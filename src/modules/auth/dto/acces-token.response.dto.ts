import { ApiProperty } from '@nestjs/swagger';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';

export class AccessTokenResponseDto {
    @ApiProperty(apiPropertyOptions.JWT_TOKEN)
    accessToken: string;
}
