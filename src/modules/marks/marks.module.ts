import { Module } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthGuard } from '../auth/guards/auth.guard';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Student } from '../students/entitites/student.entity';
import { Mark } from './entities/mark.entity';
import { MarksController } from './marks.controller';
import { MarksService } from './marks.service';

@Module({
    imports: [TypeOrmModule.forFeature([Mark, Student, Lector, Course])],
    controllers: [MarksController],
    providers: [MarksService, AuthGuard, JwtService],
})
export class MarksModule {}
