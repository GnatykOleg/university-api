import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CoreEntity from '../../app/entitites/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Student } from '../../students/entitites/student.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
    @ApiProperty(apiPropertyOptions.MARK)
    @Column({ type: 'integer', nullable: false })
    mark: number;

    @ManyToOne(() => Course, (course) => course.marks, {
        eager: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'course_id' })
    course: Course;

    @ApiProperty(apiPropertyOptions.COURSE_ID)
    @Column({ type: 'uuid', name: 'course_id' })
    courseId: string;

    @ManyToOne(() => Student, (student) => student.marks, {
        eager: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'student_id' })
    student: Student;

    @ApiProperty(apiPropertyOptions.STUDENT_ID)
    @Column({ type: 'uuid', name: 'student_id' })
    studentId: string;

    @ManyToOne(() => Lector, (lector) => lector.marks, {
        eager: false,
        onDelete: 'CASCADE',
    })
    @JoinColumn({ name: 'lector_id' })
    lector: Lector;

    @ApiProperty(apiPropertyOptions.LECTOR_ID)
    @Column({ type: 'uuid', name: 'lector_id' })
    lectorId: string;
}
