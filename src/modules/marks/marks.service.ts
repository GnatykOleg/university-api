import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';

import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Student } from '../students/entitites/student.entity';
import { CreateMarkRequestDto } from './dto/create-mark.request.dto';
import { FindAllMarksQueryDto } from './dto/find-all-marks.query.dto';
import { Mark } from './entities/mark.entity';
import { IUpdateMarkByIdParams } from './interfaces/update-mark-by-id.params.interface';

@Injectable()
export class MarksService {
    constructor(
        @InjectRepository(Student)
        private readonly studentsRepository: Repository<Student>,

        @InjectRepository(Mark)
        private readonly marksRepository: Repository<Mark>,

        @InjectRepository(Lector)
        private readonly lectorsRepository: Repository<Lector>,

        @InjectRepository(Course)
        private readonly coursesRepository: Repository<Course>,
    ) {}

    async findAllMarks(query: FindAllMarksQueryDto): Promise<Mark[] | []> {
        const {
            sort_field,
            sort_order,

            page,
            per_page,
            course_id,
            lector_id,
            student_id,
        } = query;

        const allMarks = await this.marksRepository.find({
            where: {
                courseId: course_id,
                lectorId: lector_id,
                studentId: student_id,
            },

            order: { [sort_field]: sort_order },

            skip: per_page * (page - 1),
            take: per_page,
        });

        return allMarks;
    }

    async findMarkById(markId: string): Promise<Mark> {
        const foundMark: Mark | null = await this.marksRepository.findOneBy({
            id: markId,
        });

        if (!foundMark)
            throw new NotFoundException(HttpMessagesEnum.MARK_NOT_FOUND);

        return foundMark;
    }

    async createMark(
        createMarkRequestDto: CreateMarkRequestDto,
    ): Promise<Mark> {
        const { courseId, lectorId, studentId } = createMarkRequestDto;

        const foundStudent: Student | null =
            await this.studentsRepository.findOneBy({ id: studentId });

        if (!foundStudent)
            throw new NotFoundException(HttpMessagesEnum.STUDENT_NOT_FOUND);

        const foundLector: Lector | null =
            await this.lectorsRepository.findOneBy({ id: lectorId });

        if (!foundLector)
            throw new NotFoundException(HttpMessagesEnum.LECTOR_NOT_FOUND);

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({ id: courseId });

        if (!foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        const createdMark = this.marksRepository.save(createMarkRequestDto);

        return createdMark;
    }

    async updateMarkById({
        markId: id,
        updateMarkByIdRequestDto,
    }: IUpdateMarkByIdParams): Promise<Mark> {
        const { courseId, lectorId, studentId } = updateMarkByIdRequestDto;

        const foundMark: Mark | null = await this.marksRepository.findOneBy({
            id,
        });

        if (!foundMark)
            throw new NotFoundException(HttpMessagesEnum.MARK_NOT_FOUND);

        const foundStudent: Student | null =
            await this.studentsRepository.findOneBy({ id: studentId });

        if (studentId && !foundStudent)
            throw new NotFoundException(HttpMessagesEnum.STUDENT_NOT_FOUND);

        const foundLector: Lector | null =
            await this.lectorsRepository.findOneBy({ id: lectorId });

        if (lectorId && !foundLector)
            throw new NotFoundException(HttpMessagesEnum.LECTOR_NOT_FOUND);

        const foundCourse: Course | null =
            await this.coursesRepository.findOneBy({ id: courseId });

        if (courseId && !foundCourse)
            throw new NotFoundException(HttpMessagesEnum.COURSE_NOT_FOUND);

        await this.marksRepository.update(id, updateMarkByIdRequestDto);

        const mark = this.marksRepository.findOneBy({ id });

        return mark;
    }

    async deleteMarkById(markId: string): Promise<void> {
        const deletedmark: DeleteResult = await this.marksRepository.delete(
            markId,
        );

        if (!deletedmark.affected)
            throw new NotFoundException(HttpMessagesEnum.MARK_NOT_FOUND);
    }
}
