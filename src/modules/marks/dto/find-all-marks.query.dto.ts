import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

import { SortPaginationQueryDto } from '../../app/dto/params/sort-pagination.query.dto';

export class FindAllMarksQueryDto extends SortPaginationQueryDto {
    @ApiPropertyOptional()
    @IsUUID()
    @IsOptional()
    course_id?: string | undefined;

    @ApiPropertyOptional()
    @IsUUID()
    @IsOptional()
    lector_id?: string | undefined;

    @ApiPropertyOptional()
    @IsUUID()
    @IsOptional()
    student_id?: string | undefined;
}
