import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString, IsUUID, Max, Min } from 'class-validator';

import * as apiPropertyOptions from '../../app/constants/api-property-options.constant';
import CLASS_VALIDATOR_VALUES from '../../app/constants/class-validator-values.constant';

export class CreateMarkRequestDto {
    @ApiProperty(apiPropertyOptions.COURSE_ID)
    @IsUUID()
    @IsString()
    courseId: string;

    @ApiProperty(apiPropertyOptions.STUDENT_ID)
    @IsUUID()
    @IsString()
    studentId: string;

    @ApiProperty(apiPropertyOptions.LECTOR_ID)
    @IsUUID()
    @IsString()
    lectorId: string;

    @ApiProperty(apiPropertyOptions.MARK)
    @IsInt()
    @Min(CLASS_VALIDATOR_VALUES.INT_MARK_MIN)
    @Max(CLASS_VALIDATOR_VALUES.INT_MARK_MAX)
    mark: number;
}
