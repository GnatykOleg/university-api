import { PartialType } from '@nestjs/swagger';

import { CreateMarkRequestDto } from './create-mark.request.dto';

export class UpdateMarkByIdRequestDto extends PartialType(
    CreateMarkRequestDto,
) {}
