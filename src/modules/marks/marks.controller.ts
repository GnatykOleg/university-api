import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UseGuards,
} from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiBearerAuth,
    ApiBody,
    ApiInternalServerErrorResponse,
    ApiNoContentResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiOperation,
    ApiParam,
    ApiTags,
    ApiUnauthorizedResponse,
    getSchemaPath,
} from '@nestjs/swagger';

import ENDPOINTS from '../app/constants/endpoints.constant';
import { InternalServerErrorResponseDto } from '../app/dto/errors/internal-server-error.response.dto';
import { NotFoundErrorResponseDto } from '../app/dto/errors/not-found-error.response.dto';
import { UnauthorizeErrordResponseDto } from '../app/dto/errors/unauthorized-error.response.dto';
import { ValidationFailedErrorResponseDto } from '../app/dto/errors/validation-failed-error.response.dto';
import { IdParamDto } from '../app/dto/params/id-param.dto';
import { HttpMessagesEnum } from '../app/enums/http-messages.enum';
import { generateApiParamOptionsForId } from '../app/utils';
import { AuthGuard } from '../auth/guards/auth.guard';
import { CreateMarkRequestDto } from './dto/create-mark.request.dto';
import { FindAllMarksQueryDto } from './dto/find-all-marks.query.dto';
import { UpdateMarkByIdRequestDto } from './dto/update-mark.request.dto';
import { Mark } from './entities/mark.entity';
import { MarksService } from './marks.service';

const findAllMarksApiOkResponseSchema = {
    oneOf: [
        {
            type: 'array',
            items: { $ref: getSchemaPath(Mark) },
        },
        {
            type: 'array',
            default: [],
        },
    ],
};

@UseGuards(AuthGuard)
@ApiBearerAuth('access-token')
@ApiTags('Marks')
@Controller(ENDPOINTS.MARKS)
export class MarksController {
    constructor(private readonly marksService: MarksService) {}

    // Find All Student Marks By Student Id
    @ApiOperation({ summary: 'Find all marks with filter query' })
    @ApiOkResponse({
        description: 'The marks has been successfully found',
        schema: findAllMarksApiOkResponseSchema,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get()
    async findAllMarks(
        @Query() query: FindAllMarksQueryDto,
    ): Promise<Mark[] | []> {
        const allMarks = await this.marksService.findAllMarks(query);

        return allMarks;
    }

    // Find Mark By Id
    @ApiOperation({ summary: 'Find mark by id' })
    @ApiParam(generateApiParamOptionsForId('mark', 'markId'))
    @ApiOkResponse({
        description: 'The mark has been successfully found',
        type: Mark,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.MARK_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Get(':id')
    async findMarkById(@Param() params: IdParamDto): Promise<Mark> {
        const markId = params.id;

        const foundMark = await this.marksService.findMarkById(markId);

        return foundMark;
    }

    // Create mark
    @ApiOperation({ summary: 'Create mark' })
    @ApiOkResponse({
        description: 'The mark has been successfully created',
        type: Mark,
    })
    @ApiNotFoundResponse({
        description: `${HttpMessagesEnum.STUDENT_NOT_FOUND} / ${HttpMessagesEnum.LECTOR_NOT_FOUND} / ${HttpMessagesEnum.COURSE_NOT_FOUND}`,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Post()
    async createMark(
        @Body() createMarkRequestDto: CreateMarkRequestDto,
    ): Promise<Mark> {
        const createdMark = await this.marksService.createMark(
            createMarkRequestDto,
        );

        return createdMark;
    }

    // Update mark By Id
    @ApiOperation({ summary: 'Update mark by id' })
    @ApiBody({ required: false, type: UpdateMarkByIdRequestDto })
    @ApiParam(generateApiParamOptionsForId('mark', 'markId'))
    @ApiOkResponse({
        description: 'The mark has been successfully updated',
        type: Mark,
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.MARK_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Patch(':id')
    async updateMarkById(
        @Param() params: IdParamDto,
        @Body() updateMarkByIdRequestDto: UpdateMarkByIdRequestDto,
    ): Promise<Mark> {
        const markId = params.id;

        const updatedMark = await this.marksService.updateMarkById({
            markId,
            updateMarkByIdRequestDto,
        });

        return updatedMark;
    }

    // Delete mark By Id
    @ApiOperation({ summary: 'Delete mark by id' })
    @ApiParam(generateApiParamOptionsForId('mark', 'markId'))
    @ApiNoContentResponse({
        description: 'The mark has been successfully deleted',
    })
    @ApiNotFoundResponse({
        description: HttpMessagesEnum.MARK_NOT_FOUND,
        type: NotFoundErrorResponseDto,
    })
    @ApiBadRequestResponse({
        description: HttpMessagesEnum.VALIDATION_FAILED,
        type: ValidationFailedErrorResponseDto,
    })
    @ApiUnauthorizedResponse({
        description: HttpMessagesEnum.AUTH_FAILED,
        type: UnauthorizeErrordResponseDto,
    })
    @ApiInternalServerErrorResponse({
        description: HttpMessagesEnum.ISE,
        type: InternalServerErrorResponseDto,
    })
    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteGroupById(@Param() params: IdParamDto): Promise<void> {
        const markId = params.id;

        await this.marksService.deleteMarkById(markId);
    }
}
