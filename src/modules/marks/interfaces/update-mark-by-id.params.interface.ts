import { UpdateMarkByIdRequestDto } from '../dto/update-mark.request.dto';

export interface IUpdateMarkByIdParams {
    markId: string;
    updateMarkByIdRequestDto: UpdateMarkByIdRequestDto;
}
