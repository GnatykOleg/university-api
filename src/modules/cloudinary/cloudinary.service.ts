import { BadRequestException, Injectable } from '@nestjs/common';
import {
    UploadApiErrorResponse,
    UploadApiResponse,
    v2 as cloudinary,
} from 'cloudinary';
import sizeOf from 'image-size';
import path from 'path';
import streamifier from 'streamifier';

import { CLOUDINARY_FOLDER_NAME } from './constants/cloudinary.constant';

type CloudinaryResponseType = UploadApiResponse | UploadApiErrorResponse;

@Injectable()
export class CloudinaryService {
    uploadFile(file: Express.Multer.File): Promise<CloudinaryResponseType> {
        const allowedExtensions = ['.jpg', '.jpeg', '.png'];

        const maxFileSizeInBytes = 10485760;
        const minImageWidth = 400;
        const minImageHeight = 400;

        const fileExtension = path.extname(file.originalname).toLowerCase();

        if (!allowedExtensions.includes(fileExtension))
            return Promise.reject(
                new BadRequestException('Only jpg and png files are allowed'),
            );

        if (file.size > maxFileSizeInBytes)
            return Promise.reject(
                new BadRequestException(`Max file size: 10mb`),
            );

        const dimensions = sizeOf(file.buffer);

        if (
            dimensions.width < minImageWidth ||
            dimensions.height < minImageHeight
        )
            throw new BadRequestException(
                'Image dimensions must be at least 400x400 pixels',
            );

        const uploadOptions = {
            folder: CLOUDINARY_FOLDER_NAME,
            use_filename: true,
            transformation: [{ width: 400, height: 400, crop: 'fill' }],
        };

        return new Promise<CloudinaryResponseType>((resolve, reject) => {
            const uploadStream = cloudinary.uploader.upload_stream(
                uploadOptions,
                (error, result) => {
                    if (error) return reject(error);
                    resolve(result);
                },
            );

            streamifier.createReadStream(file.buffer).pipe(uploadStream);
        });
    }

    deleteImage(publicId: string): Promise<unknown> {
        return new Promise((resolve, reject) => {
            cloudinary.uploader.destroy(publicId, (error, result) => {
                if (error) return reject(error);
                resolve(result);
            });
        });
    }
}
