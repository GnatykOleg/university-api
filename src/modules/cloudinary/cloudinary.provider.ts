import { ConfigService } from '@nestjs/config';
import { ConfigOptions, v2 as cloudinary } from 'cloudinary';

export const CloudinaryProvider = {
    inject: [ConfigService],
    provide: 'Cloudinary',
    useFactory: (confingService: ConfigService): ConfigOptions =>
        cloudinary.config(confingService.get('cloudinary')),
};
