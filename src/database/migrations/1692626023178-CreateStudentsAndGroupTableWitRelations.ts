import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateStudentsAndGroupTableWitRelations1692626023178
    implements MigrationInterface
{
    name = 'CreateStudentsAndGroupTableWitRelations1692626023178';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "students" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying(50) NOT NULL, "surname" character varying(50) NOT NULL, "email" character varying(50) NOT NULL, "age" integer NOT NULL, "image_path" character varying(350), "group_id" uuid, CONSTRAINT "PK_7d7f07271ad4ce999880713f05e" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_b5e856b621a7b64cdf48059067" ON "students" ("name") `,
        );
        await queryRunner.query(
            `CREATE TABLE "groups" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying(50) NOT NULL, CONSTRAINT "PK_659d1483316afb28afd3a90646e" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`,
        );
        await queryRunner.query(`DROP TABLE "groups"`);
        await queryRunner.query(
            `DROP INDEX "public"."IDX_b5e856b621a7b64cdf48059067"`,
        );
        await queryRunner.query(`DROP TABLE "students"`);
    }
}
