import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateResetTokenEntityAndAddRelationToLector1693669823114
    implements MigrationInterface
{
    name = 'CreateResetTokenEntityAndAddRelationToLector1693669823114';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "reset_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "token" text NOT NULL, "user_id" uuid NOT NULL, CONSTRAINT "REL_765e2b25e88f51c41139d83ff7" UNIQUE ("user_id"), CONSTRAINT "PK_93e1171b4a87d2d0478295f1a99" PRIMARY KEY ("id"))`,
        );

        await queryRunner.query(
            `ALTER TABLE "reset_token" ADD CONSTRAINT "FK_765e2b25e88f51c41139d83ff76" FOREIGN KEY ("user_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "reset_token" DROP CONSTRAINT "FK_765e2b25e88f51c41139d83ff76"`,
        );
        await queryRunner.query(`DROP TABLE "reset_token"`);
    }
}
