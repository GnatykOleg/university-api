import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddToCourseLecotrGroupStudentIndexForSomeFields1693686463106
    implements MigrationInterface
{
    name = 'AddToCourseLecotrGroupStudentIndexForSomeFields1693686463106';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE INDEX "IDX_664ea405ae2a10c264d582ee56" ON "groups" ("name") `,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_789397b8b46b99751c4fcf4bd6" ON "students" ("surname") `,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_274c3432e24ef002c0e0310763" ON "lectors" ("name") `,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_a4467253349a775ed6f5559570" ON "lectors" ("surname") `,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_6ba1a54849ae17832337a39d5e" ON "courses" ("name") `,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `DROP INDEX "public"."IDX_6ba1a54849ae17832337a39d5e"`,
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_a4467253349a775ed6f5559570"`,
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_274c3432e24ef002c0e0310763"`,
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_789397b8b46b99751c4fcf4bd6"`,
        );
        await queryRunner.query(
            `DROP INDEX "public"."IDX_664ea405ae2a10c264d582ee56"`,
        );
    }
}
