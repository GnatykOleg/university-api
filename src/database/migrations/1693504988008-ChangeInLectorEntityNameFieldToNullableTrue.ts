import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeInLectorEntityNameFieldToNullableTrue1693504988008
    implements MigrationInterface
{
    name = 'ChangeInLectorEntityNameFieldToNullableTrue1693504988008';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "lectors" ALTER COLUMN "name" DROP NOT NULL`,
        );

        await queryRunner.query(
            `ALTER TABLE "lectors" ADD "surname" character varying(50)`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "lectors" ALTER COLUMN "name" SET NOT NULL`,
        );

        await queryRunner.query(`ALTER TABLE "lectors" DROP COLUMN "surname"`);
    }
}
