import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './modules/app/app.module';
import ENDPOINTS from './modules/app/constants/endpoints.constant';
import logger from './modules/app/middlewares/logger.middleware';

async function bootstrap(): Promise<void> {
    const app = await NestFactory.create(AppModule, { cors: true });

    app.use(logger);

    app.useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            forbidNonWhitelisted: true,
            transform: true,
        }),
    );

    const config = new DocumentBuilder()
        .setTitle('University API')
        .setDescription('The university job simulation API')
        .setVersion('1.0')
        .addBearerAuth(
            {
                description: 'Enter JWT token',
                name: 'Authorization',
                scheme: 'Bearer',
                type: 'http',
                in: 'Header',
            },
            'access-token',
        )
        .build();

    const document = SwaggerModule.createDocument(app, config);

    SwaggerModule.setup(ENDPOINTS.SWAGGER, app, document);

    const configService = app.get(ConfigService);

    const APP_PORT = configService.get<number>('app.port');

    await app.listen(APP_PORT, () =>
        console.log(`Server started at port: ${APP_PORT}`),
    );
}

bootstrap();
