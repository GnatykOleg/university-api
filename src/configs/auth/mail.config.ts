import { registerAs } from '@nestjs/config';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

const { MAIL_HOST, MAIL_USER, MAIL_PASSWORD, MAIL_FROM, MAIL_PORT } =
    process.env;

const templatesPath = `${__dirname}/../../modules/mail/templates`;

export default registerAs('mail', () => ({
    transport: {
        host: MAIL_HOST,
        port: parseInt(MAIL_PORT, 10),
        secure: true,
        auth: {
            user: MAIL_USER,
            pass: MAIL_PASSWORD,
        },
    },
    defaults: {
        from: `"University" <${MAIL_FROM}>`,
    },
    template: {
        dir: templatesPath,
        adapter: new HandlebarsAdapter(),
        options: {
            strict: false,
        },
    },
}));
