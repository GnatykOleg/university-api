import { ConfigService } from '@nestjs/config';
import { MailerOptions } from '@nestjs-modules/mailer';

const maiLAsyncFactoryConfig = {
    inject: [ConfigService],

    useFactory: async (confingService: ConfigService): Promise<MailerOptions> =>
        confingService.get<MailerOptions>('mail'),
};

export default maiLAsyncFactoryConfig;
