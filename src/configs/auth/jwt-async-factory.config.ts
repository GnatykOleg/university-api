import { ConfigService } from '@nestjs/config';
import { JwtModuleAsyncOptions, JwtModuleOptions } from '@nestjs/jwt';

const jwtAsyncFactoryConfig: JwtModuleAsyncOptions = {
    inject: [ConfigService],

    useFactory: async (
        confingService: ConfigService,
    ): Promise<JwtModuleOptions> => ({
        global: true,
        secret: confingService.get<string>('auth.jwtSecret'),
        signOptions: { expiresIn: '3600s' },
    }),
};

export default jwtAsyncFactoryConfig;
