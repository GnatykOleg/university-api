import { registerAs } from '@nestjs/config';

const { APP_PORT } = process.env;

export default registerAs('app', () => ({
    port: parseInt(APP_PORT, 10) || 4000,
}));
