import { DataSource } from 'typeorm';

import { databaseConfig } from './database.config';

export const connectionSource: DataSource = new DataSource(databaseConfig);
