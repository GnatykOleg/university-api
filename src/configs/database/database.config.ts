import { registerAs } from '@nestjs/config';
import { DataSourceOptions } from 'typeorm';

const {
    DATABASE_PORT,
    DATABASE_HOST,
    DATABASE_USER,
    DATABASE_PASSWORD,
    DATABASE_NAME,
} = process.env;

const entitiesPath = `${__dirname}/../../**/*.entity{.ts,.js}`;

const migrationPath = `${__dirname}/../../**/migrations/*{.ts,.js}`;

export const databaseConfig: DataSourceOptions = {
    type: 'postgres',
    host: DATABASE_HOST || 'localhost',
    port: parseInt(DATABASE_PORT, 10),
    username: DATABASE_USER,
    password: DATABASE_PASSWORD,
    database: DATABASE_NAME,

    entities: [entitiesPath],
    migrations: [migrationPath],
    migrationsTableName: 'migrations',
    migrationsRun: true,

    synchronize: false,
    logging: true,
};

export default registerAs('database', () => databaseConfig);
