import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { DataSourceOptions } from 'typeorm';

const typeOrmAsyncFactoryConfig: TypeOrmModuleAsyncOptions = {
    inject: [ConfigService],

    useFactory: async (
        confingService: ConfigService,
    ): Promise<DataSourceOptions> =>
        confingService.get<DataSourceOptions>('database'),
};

export default typeOrmAsyncFactoryConfig;
