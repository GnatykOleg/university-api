module.exports = {
  // Parser to make ESLint work with typescript
  parser: '@typescript-eslint/parser',

  // We designate the main .eslintrc file so that the linter does not look further
  root: true,

  // Specifying parser options
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
  },

  // For which environments the script is intended
  env: {
    browser: true,
    node: true,
    es6: true,
    jest: true,
  },

  // Ignore
  // Tells ESLint to load installed plugins (we omit "eslint-plugin" from the names).
  // This allows typescript-eslint rules to be used in code.
  plugins: ['@typescript-eslint', 'prettier', 'simple-import-sort', 'import'],

  // Tells ESLint that the configuration from plugins should be extended.
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
  ],

  // Rules
  rules: {
    '@typescript-eslint/no-explicit-any': 'warn',
    '@typescript-eslint/no-empty-interface': 'warn',
    '@typescript-eslint/no-empty-function': 'warn',
    '@typescript-eslint/explicit-function-return-type': 'warn',
    '@typescript-eslint/explicit-module-boundary-types': 'warn',
    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',
    'import/first': 'error',
    'import/newline-after-import': 'error',
    'import/no-duplicates': 'error',

    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'interface',
        prefix: ['I'],
        format: ['PascalCase'],
      },
      {
        selector: 'typeAlias',
        suffix: ['Type'],
        format: ['PascalCase'],
      },
      {
        selector: 'enum',
        suffix: ['Enum'],
        format: ['PascalCase'],
      },
      {
        selector: 'class',
        format: ['PascalCase'],
      },
    ],

    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto',
      },
    ],
  },
};
